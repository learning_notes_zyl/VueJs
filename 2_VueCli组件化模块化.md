# Vue 组件模块化编程

组件组件化、模块模块化、组件通信方式、webpack 工程化、VueCLI脚手架。

- 传统方式：编写应用存在的问题依赖关系混乱，不好维护，代码复用率不高
- 模块: 向外提供特定功能的js程序，一般是个个js文件，作用复用js,简化js的编写、提高js运行效率
- 组件: 用来实现局部(特定)功能效果的代码集合 (html/css/js/image...) =》 Component =》封装狂魔

那么为什么引入组件？一个界面的功能很复杂，作用可以复用编码、简化项目编码、提高运行效率。

##1. 为什么用组件

- 提供了一种抽象，开发出一个个独立可复用的小组件来构造应用，任何的应用都会被抽象成一颗组件树。
- 尽可能的将页面拆分成一个个小的、可复用的组件，这样让代码更加方便组织和管理且扩展性也更强。
- 模块化：当应用中的 js 都以模块来编写的，那这个应用就是一个模块化的应用。
- 组件化：当应用中的功能都是多组件的方式来编写的，那这个应用就是一个组件化的应用。 

<img src="image/2_%E7%BB%84%E4%BB%B6%E7%BC%96%E7%A8%8B%E6%96%B9%E5%BA%94%E7%94%A8.png" alt="2_组件编程方应用" style="zoom: 50%;" />

## 2. 创建注册组件

1、这里举例的都是非单文件组件的定义注册使用例子。

-  Vue.extend()：首先创建组件的构造器时传 template 代表自定义模板。

-  component(‘组件名’，‘构造器’)：将上面定义的组件构造器注册为组件。

- mounted -> Vue：最后组件必须挂载在某个 Vue 实例，否则它不会生效。

- ```html
  <!-- 注册好组件后，就可以在模板中使用 -->
  <div id="app">
    <my-cpn></my-cpn>
    <my-cpn></my-cpn>
  </div>
  
  <script>
    // 首先创建组件的构造器定义template模板 
    const cpnConstructor = Vue.extend({
      // ES6 ``模板字符串,可换行不用 + 号
      template: `
        <div>
          <h2>我是标题</h2>
          <p>我是模板内容p</p>
        </div> `
    })
  
    // 其次注册组件,参数1命名，参数2上面构造器
    Vue.component('my-cpn', cpnConstructor)
    // 上面注册组件不是在Vue实例中，属于全局组件。
    const app = new Vue({
      el: '#app', 
    	data: {}
    })
  </script>
  ```

##3. 全局局部组件

- 局部注册：new Vue 时候传入 components:{………}选项，
- 全局注册:：Vue.component( '组件名' ,定义组件的构造器)。

## 4. 模板抽离写法

- 使用 template 或 script 标签,，text / x-template类型。

- ```html
   <!-- 使用组件 把对应内容映射到div中 -->
  <div id="app">
    <cpn1></cpn1>
    <cpn2></cpn2>
  </div>
  
  	<!-- 分离template标签，设置id值-->
  <template id="cpn1">
    <div>
      <h2>我是标题1</h2>
      <p>我是template标签写的cpn1</p>
    </div>
  </template>
  
  <template id="cpn2"> 
    <div>
      <h2>我是标题2</h2>
      <p>我是template标签写的cpn2</p>
    </div>
  </template>
  
  <script>
   // 注册一个全局组件及语法糖写法 命名 'cpn1'
    Vue.component('cpn1', {
      template: '#cpn1'
    })
   
    const app = new Vue({
      el: '#app',
      data: {
        message: ''
      },
        // 注册局部组件及语法糖写法 命名'cpn2'
        components: {
          'cpn2' :{
            template: '#cpn2' 
          }
        }
    })
  </script>
  ```

## 5. 组件数据存放*

组件是一个单独功能模块的封装：template 有属于自己的HTML模板，也应该有属于自己的 data 数据。

- 组件中不能直接访问的 Vue 实例 data 配置，即使可访问，如果将所有的数据都放 Vue 实例中，
- Vue 实例就会变的非常臃肿。则 Vue 组件应该有自己保存数据的地方 ( 那么组件数据存哪里？)
- 所以 data 在组件中必须是一个函数  (面试题)，如果不是一个函数，Vue直接就会报错，其次原因是在于**Vue 让每个组件对象都返回一个新的对象，因为如果是同一个对象的，组件在多次使用后会相互影响**。

<!--计数器例子用在组件 在Vue组件中定义data和methods方法  -->

```html
<!-- 直接在模板中把封装好的组件在这使用 -->
  <div id="app">
    <cpn></cpn>
    <cpn></cpn>
  </div>

<template id="cpn">
  <div>
    <h2>当前计数: {{counter}}</h2>
    <button @click="increment">+</button>
    <button @click="decrement">-</button>
  </div>
</template>

<script>
  // 组件复用，每个对象地址不一样,都是新对象
  const obj = {
    counter: 0
  }
   // 进行注册组件且在组件中定义对的应方法
  Vue.component('cpn', {
    template: '#cpn',
    data() {
      return obj
    },
    methods: {
      increment() {
        this.counter++
      },
      decrement() {
        this.counter--
      }
    }
  })

  const app = new Vue({
    el: '#app',
    data: {
      message: ''
    }
  })
</script>
```

##  6. VueComponent()*

**1、组件本质是名为 VueComponent 构造函数，Vue.extend() 自动生成而来**。

- 下面写<school/ > 或双标签组件, Vue解析时会帮创建此组件的实例对象,
- 即 Vue 帮我们执行的: new VueComponent (options)，将源码分析，
- 特别注意：每次调用 Vue.extend()，返回的都是一个全新的 VueComponent。

**2、关于组件和Vue实例的 this 指向问题**:

- 【组件中】: data函数、methods 中的函数、watch中的函数、computed中的**函数是VueComponent实例**
- 【new Vue()中】：data函数、methods中的函数、watch中的函数、computed中的**函数它们的是Vue实例**

**也就是说vue实例和组件本质都是一个组件而已，大家的属性基本是一样的，但是vue实例会比vue组件多出 【el】 和 【router】属性，而vue组件的data会被要求必须是函数，防止出现同种组件多实例共享同一个data**。

- **VueComponent的实例对象，vc (也可称之为:组件实例对象)，Vue的实例对象简称 = vm我们这里**。

- ```html
  	<div id="root">
  		<school></school>
  		<hello></hello>
  	</div>
  ...
  ```

  ```js
  const school = Vue.extend({
  		name:'school', 
  		template:`
  			<div>
  				<h2>学校名称：{{name}} </h2>	
  				<h2>学校地址：{{address}} </h2>	
  				<button @click="showName">点我提示学校名</button>
  			</div>
  		`,
  		data(){
  			return {
  				name:'yulin',
  				address:'	成都'
  			}
  		},
  		methods: {
  			showName(){
  				console.log('showName',this)
  			}
  		},
  	})
  
  	const test = Vue.extend({
  		template:`<span>test</span>`
  	})
  
  	const hello = Vue.extend({
  		template:`
  			<div>
  				<h2>{{msg}}</h2>
  				<test></test>	
  			</div>
  		`,
  		data(){
  			return {
  				msg:'你好啊！'
  			}
  		},
  		components:{ test }
  	})
  
  	const vm = new Vue({
  		el:'#root',
  		components:{
        school,
        hello
      }
  	})
  ```

  

## 7.一个重要内置关系*

1、内置关系: VueComponent.prototype._ proto__ === Vue.prototype。

- 原型间关系的的复习，注意用于添加属性方法让实例能够共享。

- ```js
      // 构造函数
          function Demo() {
              this.a = 1
              this.b = 2
          }
          // 实例对象
          const d = new Demo()
  
          // 构造函数的显示原型 Object
          console.log(Demo.prototype) 
          // 实例d的隐式原型 Object
          console.log(d.__proto__)
          
          console.log(Demo.prototype === d.__proto__)
  
  				// 在原型上添加共享方法
          Demo.prototype.x = 99 
          console.log(d.x);//99
  ```

2、为什么要有这个关系: 让组件实例对象(vc) 可访问到 Vue 原型上的属性方法.

<img src="image/2_%E9%87%8D%E8%A6%81%E5%86%85%E7%BD%AE%E5%85%B3%E7%B3%BB.png" alt="2_重要内置关系" style="zoom:50%;" />

## 8. CLI 脚手架安装

1、Command-Line interface 命令行界面，比如工地脚手架，也就是它可以快速搭建开发环境。

 2、安装@vue/cli，注意如果安装不上用管理员运行 -- npm clean cache，目前脚手架基于webpack工具。

## 9. 组件间的通信

这里开始单文件组件：安装 Vetur 插件可识别单文件组件像.vue，index.html、main.js、App.vue 组件。

- 组件实例作用域	是相互独立的，意味着不同组件之间的数据无法相互引用，需要建立通信。
- 组件中 style 标签中 scoped 作用让样式在局部生效，防止冲突，像在App.vue没写则是全部应用。

## 10. 父子间的通信

在开发中，往往一些数据确实需要从上层传递到下层，如在一个页面中，我们从服务器请求到了很多的数据。其中一部分数据，并非是我们整个页面的大组件来展示的，而是需要下面的子组件进行展示，这时，并不会让子组件再次发送一个网络请求，而是直接让父组件将数据传子组件，如何进行父子组件间的通信呢？Vue 官方提到：

**通过 props:  配置项向子组件传递数据 ，通过发送自定义事件向父组件发送消息**。

（1）从父组件中给子组件的标签动绑指令: 传值，子用 props: 配置项接收数据使用即可。

- 方式一：字符串数组，数组中的字符串就是传递时的名称
- 方式二：对象，可以设置传递时的类型，也可设置默认值.
- 如果父组件给子组件传递数据函数 : <= 本质其实是子组件给父组件传递数据。
- 如果父组件给子组件传递的数据非函数 : => 本质就是父组件给子组件传递数据。

**传递注意事项**:

- props 是只读的，Vue底层会监测你对 props的修改，如果进行了修改，就会发出警告，

- 若业务需求确实需要修改，那么请复制props的内容到data中一份，然后去修改data中的数据。

- ```html
  <!--1.app根组件暂时充当父组件模板-->
  <div id="app">
    <!-- 
  				传递数据写法动绑 <cpn v-bind:cmovies="movies"></cpn>
   			 	传递数据简写绑定 :左子组件接收名,右为父组件要传递串或函数等
  	-->
    <cpn :cmessage ="message":cmovies ="movies"></cpn>
  </div>
  
  <!-- 2.子组件模板,父没给子组件传则会显示在子组件定义的默认值 -->
  <template id="cpn">
    <div>
      <ul>
        <li v-for="item in cmovies">{{item}}</li>
      </ul>
      <h2>{{cmessage}}</h2>
    </div>
  </template>
  
  <script>
  	// 2.子接收的同时限制类型和并配置没传值的默认值项 
    const cpn = {
      template: '#cpn',
      // 数组写法 props:[],
      // 对象写法 props:{},
      props: {
        cmessage: {
          type: String,
          default: 'cmsg下面的选项,设置必须传值'
          required: true
        },  
        // 类型是对象/数组, 默认值需要写成函数()
        cmovies: {
          type: Array,
          default() {
            return []
          }
        }
      },
    }
  
    // 1.根组件，暂时先充当父组件
    const app = new Vue({
      el: '#app',
      data: {
        message: '我居然成父组件了？',
        movies: ['VueJs', 'ReactJs']
      },
      components: {
        // 注册局部子组件 'cop' : cpn
        // ES6对象增强语法 cpn！！！！！
        cpn
      },
    })
  </script>
  ```

**(2 )如京东的分类菜单获取数据，就会发生事件，向父传递 (老办法可从 App 传函数回调再App处理)**

- 新方法当子组件B需要向A父组件传递数据时，可在A中给B绑定自定义事件 (事件的回调在A中发)，
- 此时就需要用上v-on指令或简写@：它不仅可用于监听DOM事件，也可用于组件的自定义事件中。

- **方式一：在子组件中，通过 $emit() 来发射组件内触发的事件，**
- **在父组件中，通过 v-on 或@来监听子组件发射的事件**。

- ```js
  <!--1.app根组件暂时充当父组件模板-->
  <div id="app">
    <!-- 监听子组件发射事件 'itemClick' 父cpnClick()-->
    <cpn @itemClick="cpnClick"></cpn>
  </div>
  
  <!--2.子组件模板-->
  <template id="cpn">
    <div>
    <!-- 如果有点击会传item到btnClick再将事件发射父组件-->
      <button v-for="item in categories"
              @click="btnClick(item)">
        {{item.name}}
      </button>
    </div>
  </template>
  <script>
    // 子组件创建
    const cpn = {
      template: '#cpn',
      data() {
        return {
          categories: [
            {id: 'aaa', name: '热门推荐'},
            {id: 'bbb', name: '手机数码'},
            {id: 'ccc', name: '家用家电'},
            {id: 'ddd', name: '电脑办公'},
          ]
        }
      },
      
      methods: {
        btnClick(item) {
        // 这里也是在用组件实例的内置方法。
        // 发射事件自定义事件名，让父组件监听">
          this.$emit('itemClick', item)
        }
      }
    }
  
    // 父组件中注册局部子组件
    const app = new Vue({
      el: '#app',
      data: {
        message: '父组件'
      },
      components: { cpn },
      methods: {
       // <cpn @itemClick="cpnClick">
       // 判断cpn子组件事件传到父组件没?
        cpnClick(item) {
          console.log('cpnClick', item);
        }
      }
    })
  </script>
  ```


方式二：在父组件中 -> 注回调要么配置在methods中，要么用箭头函数，否则 this 指向会出问题

- ```js
  <Demo ref="demo"/>
  mounted(){
  	this.$refs.xxx.$on( ' 自定义事件名' , this.test)
  }
  // 若想让自定义事件只能触发一次，可以使用once修饰符，或$once方法。
  ```

2、解绑自定义事件`:this. $off( '自定义事件名')

补充：$listeners, $attrs他们两者是组件实例的属性，可以获取到父组件给子组件传递props与自定义事件。

## 11. 全局事件总线

GlobalEventBus：一种组件间通信的方式，适用于任意组件间通信，常用于多层组件通信。

```js
import Vue from 'vue'
import App from './App.vue'
new Vue({  
	el:'#app',
  render: h => h(App),
  beforeCreate() {
 	 Vue.prototype.$bus = this 
  },
  ...
})
```

使用：A组件想接收数据，则在A组件中给 $bus 绑定自定义事件，事件的回调留在A组件自身。

```js
	methods(){
		demo(data){......}
    
	mounted() {
		this.$bus.$on('xxxx',this.demo)
	},
 	beforeDestroy() {
    this.$bus.$off('xxxxxxxxx事件')
  }

// 注意: 在 beforeDestroy()钩子中，用$off(必须传事件) 去解绑当前组件所用到的事件.       	
```

```js
B 提供数据:this.$bus.$emit('xx事件',数据)  // 这里举例A是App，B是Item组件见todolist。
```

## 12. 消息订阅发布

1、一种组件间通信的方式，适用于任意组件间通信。

2、使用步骤；

- 安装pubsubjs库: npm i pubsub-js。

- 使用：import pubsub from ' pubsub-js。

- 订阅接收数据: A组件想接收数据，则在A组件中订阅，订阅的回调留在A组件自身。

- ```js
  methods(){
    demo(data){.....}
  }
  mounted() {
    this.pid = pubsub.subscribe('xxx',this.demo) 
  }
  ```

- 提供数据也就是发布者:  pubsub.publish('xxx' ,数据)

- 最好在beforeDestroy钩子中， 用 PubSub.unsubscribe(pid) 去取消订阅。

## 13. todolist 通信案

**组件化编码流程 (通用)** 

- 实现静态组件: 抽取组件，使用组件实现静态页面效果，
- 展示动态数据:  数据的类型、名称? 数据保存在哪组件?
- 交互——从绑定事件监听开始。

## 14. 组件实例的访问

有时候需要父组件直接访问子，子组件直接访问父或是子访问根组件。

Vue提供了相应属性用￥获取 parent、root、children、refs这些属性都挂载在组件的this上。

## 15. ref / mixin混合

1、ref 属性给元素或子组件注册引用信息 (原生 id 属性的替代者)

- 应用在 html 标签上获取的是真实DOM元素，应用在组件标签上是 vc 组件实例对象。

- ```js
  // 标识标签的方法 
  <h1 ref="xxx"></h1> 
  <School ref=" xxx"/> 
  // 组件实例方法 
  this.$refs.XXx???
  ```

2、mixins 混合/混入功能：可以把多个组件共用的配置提取成个混入对象。

- ```js
  // 第一步定义混合，例如！
  // 就像写常用配置项一样：
  {
    data(){....},
    methods:{....}
  }
  
  // 第二步使用混入，例如:
  (1).全局混入: Vue.mixin(xx)
  (2).局部混入: mixins:['xxx']
  ```

## 16. Vue plugin扩展

用于增强 Vue，本质: 包含 install 方法的 一个对象，

- install的第一 个参数是Vue，第二个以后的参数是插件使用者传递的数据。

- ```js
  obj.install = function (Vue, options) {
    // 1.添加全局过滤器
    Vue.filter(....)
    // 2.添加全局指令
    Vue.directive(....)
    // 3.配置全局混入
    Vue.mixin(....)
    // 4.添加实例方法
    Vue.prototype.$myMethod = function () {...}
    Vue.prototype.$myProperty = xXXx
   }
   // 注意要先引入再使用插件: Vue.use()
  ```

## 17. 插槽分发内容

让父组件可以向子组件指定位置插入html结构，也是种组件间通信的方式  <strong style="color:red">父 ==> 子组件</strong> 。

用于扩展，封装上抽取共性，保留不同，网上说法**：子组件内占坑，父组件里填坑(解析)**。

>  如封装个导航条每个页面的导航差不多是一样，这时用插槽灵活 (预留空间)。

1、匿名插槽无 name 属性：

```html
// 设 App 父组件中传：
...
        <Category>
           <div>html结构1</div>
        </Category>
...

//Category子组件坑：
        <template>
            <div>
               <!-- 定义插槽，组件使用者填坑 -->
               <slot>插槽默认内容...</slot>
            </div>
        </template>
```

2、具名插槽带name属性：

```html
//APP父组件中：
        <Category>
           <!-- 第一种使用方法已被第二种方法取代 --> 
            <template slot="center">
              <div>html结构1</div>
            </template>
					 <!-- 这里第二种写法, v-slot:子插槽--> 
            <template v-slot:footer>
               <div>html结构2</div>
            </template>
        </Category>

//Category子组件中：
        <template>
            <div>
               <!-- 定义具名插槽，让父组件填坑 -->
               <slot name="center">插槽默认内容...</slot>
               <slot name="footer">插槽默认内容...</slot>
            </div>
        </template>
```

## 18. 作用域插槽通信

数据在组件的自身，但根据数据生成的结构需要组件的使用者来决定。

（games数据在Category组件中，但使用数据所遍历出来的结构由App组件决定）

```html
//父组件 App 组件中
		<Category>
			<template slot="game" slot-scope="prop">  <!-- Vue2.6新增v-slot或#整合 -->
				<h4 v-for="g in prop.games" :key="g">{{g}}</h4>
			</template>
		</Category>

//子组件中：Category 
        <template> 
            <div> <slot name="game" :games="games"></slot> </div>
        </template>
		
        <script>
            export default {
                name:'Category',
                // 数据在子组件自身。
                data() {
                    return {
                        games:['红色警戒','穿越火线','劲舞团','超级玛丽']
                    }
                },
            }
        </script>
```
