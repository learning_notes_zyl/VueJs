# Vuex&Ajax网络请求库

状态管理 (Store)、Promise 对象、ajax 异步渲染和基于 Promise axios 三方库。https://vuex.vuejs.org/zh/

## 1. Vuex 集中式状态

**Vue 中实现数据管理的插件，对 Vue 应用中多个组件的共享状态进行集中式的管理**。

- Vuex 也是一种组件间通信的方式，且适用于任意组件间通信，此插件大项目用的多。
- 集成到官方调试工具，提供了诸如零配置的 time-travel 调试、状态快照导入导出等高级调试功能。
- 简单来说：就是将其看成把需要多个组件共享的变量全部存储在一个对象里，然后将这个对象放在顶层 Vue 实例中，让其他组件可使用，则多个组件就可以共享这个对象中的所有变量属性，并方便追踪管理。

**那么有什么状态需要我们在多个组件间共享的呢?**

在大型开发，一定遇到过多个状态，在多个界面间的共享问题。比如用户的登录状态、名称、头像、地理位置信息等，商品收藏、购物车中物品等。这些状态信息，我们都可以放在统一的地方管理保存且它们还是响应式的。

## 2. 单页面状态管理

![4_uex单页状态管理](image/4_uex%E5%8D%95%E9%A1%B5%E7%8A%B6%E6%80%81%E7%AE%A1%E7%90%86.png)

- state 状态：驱动应用的数据源。
- View 视图：以声明方式将 state 映射到视图。
- actions 响应：在 Vew 上的用户输入导致的状态变化。

## 3. 多页面状态管理

当我们的应用遇到多个组件共享状态时，单向数据流的简洁性很容易被破坏。

![4_uex多页状态管理](image/4_uex%E5%A4%9A%E9%A1%B5%E7%8A%B6%E6%80%81%E7%AE%A1%E7%90%86.png)

## 4. 创建/三连使用

> Vuex 和单纯的全局对象不同：Vuex 的状态存储是响应式的。当 Vue 组件从 store仓库中读取状态的时候，若 store 中的状态发生变化，那么相应的组件也会相应地得到高效更新。注意你不能直接改变 store 中状态。改变 store 中 state 状态唯一的途径就是显式地 actions对象 commit 提交到 mutation (变化)进行修改，这样我们可方便地跟踪每一个状态的变化，从而让我们能够实现一些工具帮助我们更好地了解我们的应用。

- 每一个 Vuex 应用的核心就是 store（仓库大管家）它包含着你的应用中大部分的状态，
- 将共享的状态抽取出来交给大管家，统一进行管理，之后每个视图按照规定，进行读写。

1. 安装插件  `npm install vuex --save`，创建 `src/store/index.js`目录:

   ```js
   // 引入Vue核心库
   import Vue from 'vue'
   // 引入Vuex
   import Vuex from 'vuex'
   // 应用Vuex插件
   Vue.use(Vuex)
   
   // 准备state对象——保存具体的数据
   const state = {}
   // 准备mutations对象——修改state中的数据(欸一修改方法)
   const mutations = {}
   // 准备actions对象——响应组件中用户的动作(业务和异步等)。
   const actions = {}
   
   // 创建实例并暴露配置好的Store
   export default new Vuex.Store({
   	actions,
   	mutations,
   	state
   })
   ```

2. 在```main.js```中创建 vm 时传入 store 目录仓库项

   ```js
   import store from './store'
   new Vue({...	store})
   ```

3. 初始化数据、配置```actions```、配置```mutations```，

   ```js
   // store/index.js
   import Vue from 'vue'
   import Vuex from 'vuex'
   Vue.use(Vuex) 
   
   // 在state中初始化数据
   const state = {
      sum:1
   }
   const actions = {
     // 响应组件中加的动作
   	jia (context,value){
   	// console.log('actions中的jia被调用了',miniStore,value)
   		context.commit('JIA',value)
   	},
   }
   
   const mutations = {
     // 修改state数据，注意commit提交的函数名字
   	JIA (state,value){
   		// console.log('mutations中的JIA被调用了',state,value)
   		state.sum += value
   	}
   }
   
   // 创建仓库实例并暴露
   export default new Vuex.Store({
   	actions,
   	mutations,
   	state,
   })
   ```

4. 组件中读取 vuex 中的数据：```$store.state.sum```

5. 组件中修f vuex 中的数据：

   - ```$store.dispatch('action中的方法名',数据)``` 
   - ```$store.commit('mutations中的方法名',数据)```

若没有`网络请求`或其他业务逻辑，组件中可越过 `actions`，即不写```dispatch```，直接编写```commit```, 注意了，==如果有异步任务定时器网络请求等，在actions好，开发工具可监测到，而在mutation则监测不到无法跟踪==。

## 5. getters 加工项

1. 概念：当state中的数据需要经过加工后再使用时，可用 getters 加工。

2. 在```store.js```中追加```getters```配置

```js
...
const getters = {
	bigSum(state){
		return state.sum * 10
	}
}

// 创建并暴露store
export default new Vuex.Store({
	......
	getters
})
```

3. 组件中读取数据：```$store.getters.bigSum```

## 6. map 映射使用

**用这些方法的时候，在使用的组件需要 mport {} from vuex..**。

1. mapState方法：用于帮助我们映射`state`中的数据为计算属性

   ```js
    computed: {
        //借助mapState生成计算属性：sum、school、subject（对象写法）
         ...mapState({sum:'sum',school:'school',subject:'subject'}),
             
        //借助mapState生成计算属性：sum、school、subject（数组写法）
        ...mapState(['sum','school','subject']),
    },
   ```

2. mapGetters方法：用于帮助我们映射`getters`中的数据为计算属性

   ```js
    computed: {
        //借助mapGetters生成计算属性：bigSum（对象写法）
        ...mapGetters({bigSum:'bigSum'}),
    
        //借助mapGetters生成计算属性：bigSum（数组写法）
        ...mapGetters(['bigSum'])
    },
   ```

3. mapActions方法：帮助我们生成与`actions`对话的方法，即包含`$store.dispatch(xxx)`的函数

   ```js
    methods:{
        // 靠mapActions生成：incrementOdd、incrementWait（对象形式）
        ...mapActions({incrementOdd:'jiaOdd',incrementWait:'jiaWait'})
    
        // 靠mapActions生成：incrementOdd、incrementWait（数组形式）
        ...mapActions(['jiaOdd','jiaWait'])
    }
   ```

4. mapMutations方法：帮助生成与`mutations`对话的方法，即包含`$store.commit(xxx)`的函数

   ```js
    methods:{
        // 靠mapActions生成：increment、decrement（对象形式）
        ...mapMutations({increment:'JIA',decrement:'JIAN'}),
        
        // 	靠mapMutations生成：JIA、JIAN（对象形式）
        ...mapMutations(['JIA','JIAN']),
    }
   ```

`mapActions` 与 `mapMutations`使用时：

- 若需要传递参数需要：在模板中绑定事件时传递好参数，否则参数是事件对象。。。。。。。。。

## 7. 模块化 + 命名

1. 目的：让代码更好维护，让多种数据分类更加明确。

2. 修改```store.js```

   ```javascript
   const countAbout = {
     namespaced: true,
     state:{x: 1, y: 2},
     mutations: { ... },
     actions: { ... },
     getters: {
       bigSum(state){
          return state.sum * 10
       }
     }
   }
   
   const personAbout = {
     namespaced:true,
     state:{ ... },
     mutations: { ... },
     actions: { ... }
   }
   
   const store = new Vuex.Store({
     modules: {
       countAbout,
       personAbout
     }
   })
   ```
   
3. 开启命名空间后，组件中读取` state` 数据：

   ```js
   // 方式一：自己直接读取
   this.$store.state.personAbout.list
   // 方式二：借助mapState读取：
   ...mapState('countAbout',['sum','school','subject']),
   ```

4. 开启命名空间后，组件中读取  `getters`数据：

   ```js
   // 方式一：自己直接读取
   this.$store.getters['personAbout/firstPersonName']
   // 方式二：借助mapGetters读取：
   ...mapGetters('countAbout',['bigSum'])
   ```

5. 开启命名空间后，组件中调用 `dispatch`

   ```js
   // 方式一：自己直接dispatch
   this.$store.dispatch('personAbout/addPersonWang',person)
   // 方式二：借助mapActions：
   ...mapActions('countAbout',{incrementOdd:'jiaOdd',incrementWait:'jiaWait'})
   ```

6. 开启命名空间后，组件中调用 `commit`

   ```js
   // 方式一：自己直接commit
   this.$store.commit('personAbout/ADD_PERSON',person)
   // 方式二：借助mapMutations：
   ...mapMutations('countAbout',{increment:'JIA',decrement:'JIAN'}),
   ```

## 8. AJAX异步请求

传统网站中存在的问题：网速慢的情况，页面加载时间长，用户只能等待，表单提交后，如果一项内容不合格，要重新填写所有表单内容，页面跳转重新加载页面，造成资源浪费，增加用户等待时间，体验差。

 工作原理: 它是浏览器提供的一套方法，可实现页面无刷新更新数据(异步)，提高用户浏览网站应用的体验。

![4_AJAX请求原理](image/4_AJAX%E8%AF%B7%E6%B1%82%E5%8E%9F%E7%90%86.png)

## 9.  ajax VS HTTP

**ajax 请求是一种特别的 http请求，对服务器端来说没有任何区别,，但区别在浏览器端**

**浏览器端发请求: 只有 XHR 或 fetch 发出的才是 ajax 请求,，其它所有的都非 ajax 请求**。

- **一般请求: 浏览器一般会直接显示响应体的数据,，也就是我们常说的刷新 / 跳转页面.。**
- **ajax 请求: 浏览器不会对界面进行任何更新操作,，只是调用监视的回调函数并传入响应相关数据**.。

## 10. 原生的请求例子

```js
  var btn = document.getElementsByTagName('button')[0]
    btn.onclick = function () {
      var xhr = new XMLHttpRequest()
      xhr.open('GET', 'da.json', true)
      console.log(xhr);
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log(xhr.responseText);
        }
     }
    xhr.send()
  }
```

##  11. XHR请简单封装

```javascript
/* 
    1,使用 XHR 封装发送 ajax 请求的通用函数 
    2.思想：函数的返回 promise,成功response,失败 error 了
    3.能处理多种类型的请求: GET/POST/PUT/DELETE
    4.函数的参数为一个配置对象
    {
        url: '',     // 请求地址
        method: '',  // 请求方式GET/POST/PUT/DELETE
        params: {},  // GET/DELETE请求的 query 参数
        data: {},    // POST或 DELETE请求的请求体参数 
    }
    5.响应json数据，自动解析为 js的对象/数组。  
*/
function myAjax({ url, params = {}, data = {}, method = 'GET' }) {
    return new Promise((resolve, reject) => {
        // 创建一个 XHR 对象与服务器交互
        const request = new XMLHttpRequest()

        // 根据 params 拼接 query参数（?id=1&xxx=abc）
        let queryStr = Object.keys(params).reduce((pre, key) => {
            pre += `&${key}=${params[key]}`
            return pre
        }, '')
        
        if (queryStr.length > 0) {
            queryStr = queryStr.substring(1)
            url += '?' + queryStr
        }

        // 请求方式转换为大写得格式如GET
        method = method.toUpperCase()

        // 初始化一个异步请求(未发请求)
        request.open(method, url, true)
        // 绑定请求状态改变的监听函数
        request.onreadystatechange = function () {
            // 如果状态值不为4, 直接结束(请求还没有结束)
            if (request.readyState !== 4) {
                return
            }

            // 如果响应码在200~~299之间, 说明请求都是成功的
            if (request.status === 200 && request.status < 300) {
                // 准备响应数据对象
                const responseData = {
                    data: request.response,
                    status: request.status,
                    statusText: request.statusText
                }
                // 指定promise成功结果值
                resolve(responseData)
              
            } else {
                // 指定promise失败结果值
                const error = new Error('request error status ' + request.status)
                reject(error)
            }
        }

        // 指定响应数据格式为json => 内部就是自动解析
        request.responseType = 'json'
        // 如果是 post / put请求
        if (method === 'POST' || method === 'PUT') {
            // 设置请求头: 使请求体参数以 json 形式传递
            request.setRequestHeader('Content-Type', 'application/json;charset=utf-8')
            // 包含所有请求参数的对象转换为 json格式
            const dataJson = JSON.stringify(data)
            // 发送请求, 指定请求体数据
            request.send(dataJson)
        } else {
            // 发送请求 GET/DELETE请求
            request.send(null)
        }
    })
}
```

## 12. Promise异步编程

Promise 是构造函数可通过 new 来调用 Promise 的构造器来进行实例化，是异步编程的一种方案，并支持链式调用 (解决回调地狱)，从功能上说 Promise 是用来封装一个异步操作并可以获取其成功或者失败的结果的值。

> 背景: 常见网络请求，封装一个网络请求的函数，因为不能立即拿到结果，所以不能像简单的计算一样将结果返回，所以往往我们会传入另外一个函数，在数据请求成功时，将数据通过传入的函数回调出去，如果只是一个简单的网络请求，那这种方案不会带来大麻烦，但复杂时就会出现回调地狱 (代码一直向前推进! )

## 13. 三种基本状态

创建对象时，传入 relove,reject 函数固定的，根据请求数据的状态成功/失败决定调用。

基本方法和第二种解决异步的方案：

- executor (exe)、 resolve、reject、=>> 【then、try、catch、all、race等】。
- aysnc (asynchronous， sync同步) await (得到异步结果)【她们要成对出现使用】。

三种 pending、fulfill / success、reject 其特点是只有异步操作的结果能决定是那种状态：

- pending 等待状态，如正在进行网络请求 / 定时器没有到时间
- fulfill/：成功状态，当我们主动回调了resolve 时，就处于该状态，并且会回调.then()
- reject：拒绝状态，当我们主动回调了reject 时，就处于该状态，并且会回调.catch()

> 其缺点： 一旦创建就无法取消,，一旦新建就会立即执行，无法中途取消，如果不设置回调函数，它的内部错误就不会反映到外部。当处于 pending 状态时，无法得知当前处于哪一个状态，是刚刚开始还是结束？

```js
new Promise((resolve, reject) =>{
    setTimeout(() => {
      resolve("success") 
      // reject("error")
    },1000)
  // then方法获取成功/失败值！！！！！！
  }).then( value => {
    console.log(value);
  }, reason => {
    console.log(reason);
  })
```

## 14. 链式调用重点

**无论是 then 还是 catch 都可以返回个 Promise对象，所以我们的代码其实是可以进行链式调用。**

**这里我们直接通过 Promise(也就是一个容器) 包装一下新的数据，将 Promise 对象返回**；

- **Promise.resovle ()：将数据包装成 Promise对象，且在内部回调 resolve 函数**
- **Promise.reject ()：将数据包装成 Promise 对象，且在内部回调 reject 函数**
- **promise.then ()：方法返回的是一个新的 Promise 对象，参数为回调函数**

<font color="red">// 网络请求 aaa -> 自己处理10行，成功的结果用then得到处理10行再new继续……..</font>
		// 处理  aaa111 -> 自己处理10行
		// 处理  aaa111222 -> 自己处理

```js
 // 方式1，new Promise 传回调函数
new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('aaa')
      }, 1000) 
    }).then(res =>{
      console.log(res, "第一层10行代码");
      // 对结果第一次处理
      return new Promise((resolve) => {
        resolve(res + '111')
      })
    }).then(res => {
      console.log(res, "第二层得10行处理代码");  //aaa111
  //===============================j=====================================
      return new Promise((resolve) => {
        // 对结果第二次处理
        resolve(res + '222')
      })
    }).then (res =>{
      console.log(res, "第三层得10行处理代码");
    })

// 方式2 then中省去new对象，直接用Promise调方法
new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('aaa')
      }, 1000) 
    }).then(res =>{
      console.log(res, "第一层10行代码");
      return Promise.resolve(res + '111')
    })// 省略...
```

**简化：如果我们希望数据直接包装成 Promise.resolve，那么在 then 中可以直接返回数据**

- 下面代码，return Promise.resovle(data) 改成 return data 结果一样。

- 如果中间有错误直接调用 reject 或 throw抛出错误，用 catch 处理。

```js
 new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve('aaa')
            }, 1000) 
        }).then(res =>{
           // 己处理10行代码
            console.log(res, "第一层10行代码");
          // 对结果第一次的处理
     	    // 内部会promise包装 
            return res + '111' 
        })// then 省略一些代码。
/*  
		结果 aaa 第一层10行代码
		promise.html:72 aaa111 第二层得10行处理代码
		promise.html:76 aaa111222 第三层得10行处理代码
*/
```

## 15. all方法多请求

请求一: 网络请求，请求二:两个请求都完成，继续往下。

[] 多个异步请求 all方法，都成功才返回，失败直接失败。  

```js
      Promise.all([
        //1
          new Promise((resolve, reject) => {
              setTimeout(() => {
                   resolve('result1')
              },2000)
            // $ajax({
            //     ur1: 'url1',
            //     success: function (value){
            //       resolve(value)
            //     }
            // })
          }),
        //2
          new Promise((resolve, reject) => {
            // $ajax({
            //     ur1: 'url2',
            //     success: function (value){
            //       resolve(value)
            //     }
            // })
            setTimeout(() => {
                resolve('result2')
                },1000)
          })

        ]).then(results => {
            console.log(results);
        })
				
        /*  返回一个成功的数组！！！！！！！！！！！！！
        ["result1", "result2"]
        0: "result1"
        1: "result2"
        length: 2  */
```

## 16. race方法跑快

就是赛跑，.race([p1, p2,等]) 里哪个结果获得的快就返回，不管结果本身是成功状态还是失败状态。

## 17. 网络请求选择

> 引入：传统 Ajax 基于 XMLHttpRequest(XHR)，配置和调用方式等非常混乱，后来引入使用 jQuery 库Ajax，但是在 Vue 就不需要了，jQuery 代码太多 1w+，官方在Vue1.x时候, 推出了 Vue-resource，相对于 jQuery小的多，在Vue2.0后, Vue 作者在GitHub 的 Issues 中说明了去掉 vue-resource 	且不会再更新了。
>
> 跨域问题：早期 JSONP 的使用，在前端开发中，我们一种常见的网络请求方式就是 JSONP，使用JSONP最主要的原因往往是为了解决跨域访问的问题，原理核心在于通过<script>标签的 src 获取，原因是我们的项目部署在 main1.com 服务器上时, 是不能直接访问 main2.com服务器上的资料等，这个时候？
>
> 我们利用 <script> 标签的 src 帮助我们去服务器请求到数据 (也就是不受同源策略限制)。。。。。

## 18. axios 库的请求

1、轻量级 HTTP 客户端;

- 从浏览器中创建 XMLHttpRequests 对象。
- 从 node.js创建 http请求，支持 Promise API
- 拦截请求数据和响应数据
- 转换请求和响应数据，取消请求，自动转换JSON数据
- 客户端支持防护抵御 XSRF`跨站请求伪造。

2、支持多种请求方式：

- axios (config)
- axios.request (config)
- axios.get (url[, config])
- axios.delete (url[, config])
- axios.head (url[, config])
- axios.post (url[, data[, config]])
- axios.put (url[, data[, config]])
- axios.patch (url[, data[, config]]) 

## 19. axios 基本使用

1. 用 npm 安装引入axios库

   ```shell
   npm install axios --sava
   import axios from 'axios'
   ```

2. 没写请求方式，默认 GET

   ```js
    axios({
     // url: 'http://127.0.0.1:5000/home/data?type=sell&page=2',
        url: 'http://127.0.0.1:5000/home/data',    
       params: {   // 专门针对 get 请求的参数的拼接, post请求用data对象
         type: 'Sell',
         page: 2 
       }
      }).then(res => {
       // then回调得到后端返回的数据
        console.log(res);
      })
   ```

3. 发并发请求，多个请求，使用 axios.all, 可以放入多个请求的数组， 返回的结果是一个数组对象，使用 axios.spread方法（展开） 可将数组对象 [ res1,res2等] 展开为 res1, res2，类似 ES6…扩展运算符。

   ```js
   axios.all([axios({
       url: 'http://127.0.0.1:5000/home/multidata'
     }), axios({
       url: 'http://127.0.0.1:5000/home/data',
       params: {
         type: 'sell',
         page: 5
       } 
     })]).then(results => { 
       console.log(results);
       console.log(results[0]);
       console.log(results[1]);
     })
   ```

4. 全局配置选项

- 上面的示例中, 我们的 BaseURL是固定的，事实上在开发中可能很多参数都是固定

- 这个时候我们可以进行一些抽取，也可以利用 axios 的全局配置，如下两个属性！！

  ```js
  axios.defaults.baseURL = ‘127.0.0.1:5000’
  axios.defaults.headers.post[‘Content-Type’] 
    = ‘application/x-www-form-urlencoded’
  
  axios.defaults.baseURL = 'http://127.0.0.1:5000'
  axios.defaults.timeout = 5000
    // 发送多个网络请求
    axios.all([axios({
      url: '/home/multidata'
    }), axios({
      url: '/home/data',
      params: {
        type: 'sell',
        page: 5
      }
    // 结果单独取出用spread API.
    })]).then(axios.spread((res1, res2) => {
      console.log(res1);
      console.log(res2);
    }))
  ```

5. 常见配置选项

- 请求地址 url: '/user',
- 请求类型  get 传 params，post 传 data: { key: 'aa'},
- 请根路径：举例 baseURL: 'http://www.mt.com/api',
- 请求前的数据处理：transformRequest:[ function(data) {} ],
- 请求后的数据处理：transformResponse: [ function(data) {}],
- 自定义的请求头：headers:{'x-Requested-With':'XMLHttpRequest'},
- URL 查询对象(query) ? 拼接，params:{ id: 12, age:22 }等见GitHub。

## 20. 创建实例常用*

- 当我们从 axios 模块中导入对象时，使用的实例是默认的实例，当给该实例设置一些默认配置时，这些配置就被固定下来了，但是后续开发中，某些配置可能会不太一样，比如某些请求需要使用特定的 baseURL / timeout / content-Type等，这时我们就可以创建新的实例，并且传入属于该实例的配置信息。

- ```js
    // 创建对应的 axios的实例 create方法创建实例
    const instance1 = axios.create({
      baseURL: 'http://127.0.0.1:5000	',
      timeout: 5000
    })
    instance1({
      url: '/home/multidata'
    }).then(res => {
      console.log(res);
    })
    instance1({
      url: '/home/data',
      params: {
        type: 'pop',
        page: 1
      }
    }).then(res => {
      console.log(res);
    })
    
  // instance2
    const instance2 = axios.create({
      baseURL: 'http://127.0.0.1:8000/',
      timeout: 10000,
    })
  ```

## 21. 拦截器的封装*

请求拦截器，响应拦截器，用于我们在发送每次请求 / 得到响应后，进行对应的处理。

```js
//  `api/request.` axios二次封装：
import axios from 'axios'
export function request(confieg) {
    // 1. 创建axios的实例
    const instance = axios.create({
      baseURL: '基础地址http://？？？？？',
      timeout: 5000
    })

    // 2.请求拦截器，request，以下返回个Promise对象
    instance.interceptors.request.use(config => {
      // 为什么拦截？
        // 1.比如 config 中的一些信息不符合服务器的要求
        // 2.比如每次发送网络请求时, 都希望在界面中显示一个请求的图标
        // 3.某些网络请求比如登录token令牌, 必须携带一些特殊的信息呀
      //config中含请求头
      return config 
    }, err => {
       console.log(err);
    })

    // 3.响应拦截的 respond，以下返回个Promise对象
    instance.interceptors.response.use(res => {
    // 响应的成功拦截中，主要是对数据进行过滤。
    // 响应失败 (服务器返回错误码) 拦截中，根据 status 判断错误码，跳转到不同的提示页面。
      // console.log(res);
      return res.data 
    }, err => {
      console.log(err);
    })

    // 4.发送真正的网络请求 返回Promise对象
    return instance(config)
  }
```

那么封装好了如何使用 request 对象？？？？？？？

```js
  // 5.导入封装的request模块,不是默认暴露的用{}
  import {request} from "./network/request";
  // 返回 Promise,那么就可使用她的相关的API了.
  request({
    url: '/home/multidata'
  }).then(res => {
    console.log(res);
  }).catch(err => {
    // console.log(err);
  })
```

## 22. 配置代理（跨域）

[FeHelper ( chrome 浏览器插件 )](https://www.baidufe.com/fehelper/index/index.html)  JSON 美化插件工具。

https://api.github.com/search/users?q=xxx 官方免费接口,见GitHub请求例。 

**本地调试的时候中：解决AJAX请求跨域问题，跨域原因违背同源策略**。

:协议、域名、端口号不同任何一个满足请求，===》》》》》称之为跨域。

- 代理解决跨域：前有jsonp 使用script标签 src属性不受跨域限制，后台有core解决

- 现有在vue中配置代理：创建 vue.config.js 中添加如下代理配置: 

- ```js
  devServer:{
  	proxy: "http://localhost :5000"
  }
  ```

**方式一说明**：

- 优点: 配置简单，请求资源时直接发给前端 (8080) 即可。
- 缺点: 不能配置多个代理,不能灵活的控制请求是否走代理。

​	工作: 上述当请求了前端不存在的资源时，那么该请求会转发给服务器(优先匹配前端资源)

**方式二说明**：

- 优点：可配置多个代理，且可灵活的控制请求是否走代理。
- 缺点：配置略微繁琐，请求资源时必须加前缀如/api1这样。

```js
module.exports = {
	devServer: {
      proxy: {
      '/api1': {// 匹配我们设置的所有以 '/api1'开头的请求路径
        target: 'http://localhost:5000',// 代理目标基础路径
        changeOrigin: true,
        pathRewrite: {'^/api1': ''}
      },
      '/api2': {
        target: 'http://localhost:5001',
        changeOrigin: true,
        pathRewrite: {'^/api2': ''}
      }
    }
  }
}
/* changeOrigin 默认值为true.接口跨域开启就对了
   changeOrigin设置为true时，服务器收到的请求头中的host为：localhost:5000
   changeOrigin设置为false时，服务器收到的请求头中的host为：localhost:8080
   
   pathRewrite作用是因为正确的接口路径是没有/api的，所以用'^/api': '/'，表示请求接口时去掉api。
*/
```



