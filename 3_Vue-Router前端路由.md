# VueRouterSPA 应用

**https://router.vuejs.org/zh/introduction.html 官网地址**。

网页跳转 (URL改变)，静态/动态路由(	带信息)、嵌套路由、参数传递、路由懒加载、编程导航和守卫。

## 1. 前后端路由分析

- **路由就是通过互联网把信息从源地址传输到目的地址的活动**。
- 一个路由（route）就是一组映射关系（key - value）多个路由需要用路由器（router）来进行管理。
- 前端路由：key 是路径，value 是组件 (切换组件)，后端路由：value 是函数，用来处理客户端请求。

<img src="image/3_%E5%89%8D%E7%AB%AF%E8%B7%AF%E7%94%B1%E7%AB%A0%E8%8A%82.jpg" alt="3_前端路由章节" style="zoom:67%;" />

- **后端如何渲染：早期的网站开发整个HTML页面是由服务器来渲染，服务器直接生产渲染好对应的HTML页面, 返回给客户端进行展示，但是, 一个网站,，这么多页面服务器如何处理呢?**
- 一个页面有自己对应的网址, 也就是URL，URL会发送到服务器, 
- 服务器会通过正则对该URL进行匹配, 并且最后交给一个Controller控制者进行处理.
- Controller进行各种处理, 最终生成HTML或者数据, 返回给前端.这就完成了一个IO操作.

> 上面的这种操作, 就是后端路由，当我们页面中需要请求不同的路径内容时, 交给服务器来进行处理, 
>   服务器渲染好整个页面, 并且将页面返回给客户顿，这种情况下渲染好的页面, 不需要单独的去加载，
>   任何的js 和 css, 可直接交给浏览器展示,，也有利于SEO(Search Engine Optimization)搜索引擎的优化，缺点：————>通常情况下HTML代码和数据以及对应的逻辑会混在一起, 编写和维护都是非常糟糕的事情。

**// 比如说用 node.js，express框架搭建个服务器**。

```js
const express = require(' express' )
const app = express()
app.use(express.static(__dirname+'/static'))
app.get(' /personInfo' ,(req,res)=>{
    res.send({
    name: 'yulin' ,
    age :22
  })
})
app.listen(5005, (err)=>{
	if(!err) console.1og( '服务器启动成功了! ')
})
```

## 2. 来引入前端路由

(1) 前后端分离阶段：后端只提供数据，前端负责渲染页面，

- 随着Ajax的出现, 有了前后端分离的开发模式，后端只提供API来返回数据, 前端通过Ajax获取数据, 用 JS 将数据渲染到页面，这样做最大的优点就是前后端责任的清晰, 后端专注于数据上, 前端专注于交互和可视化上，且当移动端(iOS/Android)出现后**, 后端不需要进行任何处理, 依然使用之前一套API**。

(2) 单页面富应用阶段：在前后端分离的基础上加了一层前端路由 (也就是前端来维护一套路由规则)。

- 前端路由核心改变URL，但是页面不进行整体的刷新，不需要向服务器发请求。
- 在vue-router的单页面应用中, **页面的路径的改变就是组件的切换**。

## 3. URL 的的组成

统一资源定位器 (Uniform Resource Locator)

- URL: http协议://host主机:(端口可省)/path ? (查询query)
- URL可由字母组成，如"yulin.com"，或互联网协议（IP）192.66.33.50
- 大多数人进入网站使用网站 "域名" 来访问，因为名字比数字ip更容易记住。

我们要改变URL 且不让页面刷新，接下来路由配置都是为了这个。

## 4. 路由的两种模式

**hash 模式**：

1. 地址带着 # 号，兼容性较好且不会包含在 HTTP 请求中，意味着 hash 值不会带给服务器。

2. 若以后将地址通过第三方手机app分享，若app校验严格，则地址会被标记为不合法。

3. ```js
   // URL的hash也就是锚点(#和后面内容) 本质改变`window.location`的`href`属性.
   // 我们可以通过直接赋值`location.hash`来改变`href`属性值，但是页面不发生刷新。
   // href ===> hyper reference,
   	location.hash = 'hellohash'
     location.href // 得值 "http://localhost:8080/#hellohash"
   ```

**history 模式**：

1. 地址干净，美观，但是兼容性和 hash 模式相比略差。

2. 应用部署上线时需要后端人员支持，解决刷新页面服务端 404 的问题！！！！！！！！

3. ```js
   history.pushState({}, '', 'home') //  添加URL，栈有记录可返回
   history.back() /.go(-1)           // -1弹回一个。 +1则压入一个
   histor.replaceState({}, '', 'foo') // 替换的没返回按钮历史记录
   ```

## 5. vue-router插件

一级路由、多级路由、路由携带的(query、params)参数，props发送接收参数优化\、历史记录。

1. 安装vue-router，命令 `npm install vue-router `

2. 引入后应用插件：```Vue.use(VueRouter)```

3. 编写 router 配置: `router / index.js`

   ```js
   // 引入VueRouter
   import VueRouter from 'vue-router'
   
   // 注意用到的组件需要引入，路由组件一般放pages
   import About from '../pages/About'
   import Home from '../pages/Home'
   
   // 创建router实例对象，管理路由规则(路由器)
   const router = new VueRouter({
     node:'hash/h'
   	routes:[
       {
         path: '',  // 默认路由路径
         redirect: '/home' // 重定向
    		},
   		{
   			path:'/about',
   			component:About
   		},
   		{
   			path:'/home',
   			component:Home
   		}
   	]
   })
   
   // 暴露router,在main引入并挂载此路由器。
   export default router
   ```

   `main.js` 相关的配置！！！！！！

   ```js
   // 引入 vue 框架
   import Vue from 'vue'
   // 引入 App 组件
   import App from './App.vue'
   // 引入 VueRouter 
   import VueRouter from 'vue-router'
   // 路由器(管理路由)
   import router from './router'
   // 应用插件VueRouter
   Vue.use(VueRouter)
   
   new Vue({
     render: h => h(App),
     router:router,
   }).$mount('#app')
   ```

4. 实现切换及（active-class 可配置高亮样式，动态的点击谁就高亮）

   ```html
   <router-link active-class="active" to="/about">About</router-link>
   <router-link active-class="active" to="/home">Home</router-link>
   ```

5. 命名路由作用：简化上面路由的跳转(多级路由和带参数路由用的多)

   给路由命名：

   ```js
   {
   	path:'/demo',
   	component:Demo,
   	children:[
   		{
   			path:'test',
   			component:Test,
   			children:[
   				{
             name:'hello' 
   					path:'welcome',
   					component:Hello,
   				}
   			]
   		}
   	]
   }
   ```

   简化跳转：

   ```html
   <!--简化前，需要写完整的路径 -->
   <router-link to="/demo/test/welcome">跳转</router-link>
   <!--简化后，直接通过名字跳转 -->
   <router-link :to="{name:'hello'}">跳转</router-link>
   
   <!--简化后写法配合传递参数 -->
   <router-link 
   	:to="{
   		name:'hello',
   		query:{
   		   id:xxx,
          title:xxx
   		}
   	}">
     跳转</router-link>
   ```

6. 将组件展示 `router-view` 标签（点了那个link路由就会渲染那个,一般在APP）

**路由的注意点**：

- 路由组件通常存放在```pages```文件夹，一般组件存放在```components```文件夹。
- 通过切换，“隐藏”了的路由组件，默认是被销毁掉的，需要的时候再去挂载。
- 每个组件都有自己的```$route```属性，存储着自己的路由信息如**query**，**path**等。
- 整个应用只有个 router(路由器)，通过组件的```$router```属性获取到，【如push跳转导航】，常用。

## 6. 嵌套路由(多级)

- 配置路由规则，多级使用 children 配置项：

  ```js
  ...
  routes:[
  	{
  		path:'/about',
  		component:About,
  	},
  	{
  		path:'/home',
  		component:Home,
  		children:[ // 通过children配置子级路由
  			{
  				path:'news', //不要写：/news
  				component:News
  			},
  			{
  				path:'message',//不要写：/message
  				component:Message
  			}
  		]
  	}
  ]
  ```

- 跳转（要写完整路径）展示区还是用得 router-view。

  ```html
  <!-- 跳转的路径用to,不要用a的属性href了。 -->
  <router-link to="/home/news">News</router-link>
  ```

## 7. 路由query参数

1. 传递参数 

   ```vue
   <!-- 跳转并携带query参数，to的字符串写法 -->
   <router-link :to="/home/message/detail?id=666&title=你好">跳转</router-link>
   				
   <!-- 跳转并携带query参数，to的对象写法 -->
   <router-link 
   	:to="{
   		path:'/home/message/detail',
   		query:{
   		   id:666,
               title:'你好'
   		}
   	}"
   >跳转</router-link>
   ```
   
2. 接收参数(哪里使用? {{}})：：

   ```js
   $route.query.id
   $route.query.title
   ```

## 8. 路由params参数

1. 配置路由，声明接收params参数， **需要提前占位**。

   ```js
   {
   	path:'/home',
   	component:Home,
   	children:[
   		{
   			path:'news',
   			component:News
   		},
   		{
   			component:Message,
   			children:[
   				{
   					name:'hello',
             // 使用占位符声明接收params参数
   					path:'detail/:id/:title', 
   					component:Detail
   				}
   			]
   		}
   	]
   }
   ```

2. 传递参数

   ```vue
   <!-- 跳转并携带params参数，to的字符串写法 -->
   <router-link :to="/home/message/detail/666/你好">跳转</router-link>
   				
   <!-- 跳转并携带params参数，to的对象写法 -->
   <router-link 
   	:to="{
   		name:'hello', // 命名路由。
   		params:{
   		   id:666,
           title:'你好'
   		}
   	}"
   >跳转</router-link>
   ```

   > 注：路由携带params参数时，若使用to的对象写法，则不能使用path配置项，必须使用name配置！

3. 接收参数(哪里使用? {{}})：

   ```js
   $route.params.id
   $route.params.title
   ```

## 9. 路由props配置

​	作用：让路由组件更方便的收到参数。

- ```js
  {
  	name:'hello',
  	path:'detail/:id',
  	component:Detail,
  	// 第一种写法：props值为对象，该对象中所有的key-value的组合最终都会通过props传给Detail组件
  	// props:{a:900}
  
  	// 第二种：props值为布尔值，布尔值为true，则把路由收到的所有params参数通过props传给Detail组件
  	// props:true
  	
  	// 第三种写法：props值为函数，该函数返回的对象中每一组key-value都会通过props传给Detail组件!!!
  	props(route){
  		return {
  			id:route.query.id,
  			title:route.query.title
  		}
  	}
  }
  
  // 接收数据方法：props:['id','title']
  ```

## 10. router-link 声明式

第一种：声明式导航 router-link标签，

1. 作用：控制路由跳转时操作浏览器历史记录的模式
2. 浏览器的历史记录两种写入方式：`push`、`replace`，
   - ```push```是追加历史记录(默认)，
   - ```replace```是替换当前的记录(栈顶)。

开启```replace```模式：```<router-link replace >News</router-link>```

———————————-具体跳转见编程式导航——————————-

## 11.好用的 编程式导航

第二种：编程式导航不再借助 router-link跳转及缓存路由的信息实现。

1. 作用：不借助```<router-link> ```实现路由跳转，让路由跳转更加灵活

2. 具体得编码，使用组件实例上的属性 **router** ( 挂载在了个组件实例)：

   ```js
   // $router的两个API。
   this.$router.push({
   	name:'hello',
   		params:{
   			id:xxx,
   			title:xxx
   		}
   })
   
   this.$router.replace({
   	name:'hello',
   		params:{
   			id:xxx,
   			title:xxx
   		}
   })
   ```

   如下操作得有历史记录：

   ```js
   this.$router.forward() 
   this.$router.back() 
   this.$router.go() 
   ```

## 12. 缓存路由组件

让不展示的路由组件保持挂载，不被销毁，注意指定具体要缓存那个组件，路由组件很多毕竟，

```html
<!-- 比如在New组件表单输入内容了，切换回来信息还保存在表单。 -->
<!-- 这在父组件Home包裹展示的News组件，并指定具体缓存组件 -->
<keep-alive include="News"> 
    <router-view></router-view>
</keep-alive>
```

## 13. 路由的懒加载模式

当打包构建应用时，javascript 包会变得非常大，影响页面加载，甚至空白。

把不同路由对应的组件分割成不同的代码块，当路由被访问的时候才加载对应组件，更加高效。

```js
// 方式一: 结合Vue的异步组件和Webpack的代码分析.
const Home = resolve => { require.ensure(['../components/Home.vue'], () => { resolve(require('../components/Home.vue')) })};

// 方式二: AMD写法
const About = resolve => require(['../components/About.vue'], resolve);

// 方式三: 在ES6中, 组织Vue异步组件和Webpack的代码分割.import中的会动态加载。
const Home = () => import('../components/Home.vue')
```

## 14. 新增钩子函数

作用：路由组件所独有的两个钩子，用于捕获路由组件的激活状态 (在特殊的时候调用)

activated：路由组件被激活时触发、deactivated：路由组件失活时触发。

## 15. 路由守卫  (权限)

作用：对路由进行权限控制。分类：全局守卫、独享守卫、组件内守卫。

1、 全局守卫：先设置个需要的权限校验 meta

```js
...  
{
    name: 'xiaoxi',
    path: 'message',
    // 元数据，自己定义的信息
    component: Message,
    meta: {isAuth:true, title:'消息'},
    children: [{
       ...
    }]
```

全局守卫：又分为前置守卫beforeEach()*、后置守卫afterEach()

```js
// 全局前置守卫：初始化时执行、每次路由切换前执行
router.beforeEach((to, from, next) => {
    if (to.meta.isAuth) {
        if (localStorage.getItem('school') === 'yulin') { 
            next() 
        } else {
            alert('暂无权限查看， 或者重定向具体的位置')
        }
    } else {
        next() 
    }
})
```

```js
// 全局后置守卫：初始化时执行、每次路由切换后执行
// 没有next()，一般用于切换后[修改路由的标题]。
router.afterEach((to, from) => {
    console.log('afterEach', to, from)
    if (to.meta.title) {
        document.title = to.meta.title 
    } else {
        document.title = 'VUE_TITLE '
    }
})
```

2、路由独享守卫：只有独享前置 beforeEnter（），可以配合全局后置…

```js
beforeEnter(to,from,next){
	console.log('beforeEnter',to,from)
	if(to.meta.isAuth){ 
		if(localStorage.getItem('school') === 'atguigu'){
			next()
		}else{
			alert('暂无权限查看')
		}
	}else{
		next()
	}
}
```



