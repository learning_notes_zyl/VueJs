

# MVVM&数据代理及钩子

MVVM设计、常用的钩子函数、 数据代理、监视原理Observer、被vue配置监视的数组方法。

##  1. M-V-VM三层模型

- **Model数据层**：数据可能是固定死数据，更多的是来自我们服务器，从网络上请求的数据。

- **View视图层**：在我们前端开发中，通常就是DOM层，主要的作用是给用户展示各种信息。

- **VueModel层**：VM实例：视图模型层是上面两者 View 和 Model沟通的桥梁。

  - 实现了Data Binding(数据绑定)，将Model的改变实时的反应到View中

  - 实现了DOM Listener(监听)，当DOM发生事件时可被 watcher 监听到，
  - 并在需要的情况下改变对应的 Data 数据。。。。。。


<img src="image/1_MVVM%E6%A8%A1%E5%9E%8B.png" alt="1_MVVM模型" style="zoom:50%;" />

## 2. HOOK 生命周期

1、又名生命周期钩子：Vue在特殊时刻帮我们调用的一些特殊名称的函数，也可以主动操作实例`$`.钩子

- 生命周期函数的名字不可更改，具体内容程序员需求编写
- 生命周期函数中的 this 指向是 vm 或组件实例对象。

  <!-- 用间隔器setInterval和opacity实现透明持续变化效果 -->

```html
  <div id="app">
   <h2 v-if="!display">你好啊</h2>
   <h2 :style="{opacity}">欢迎学习Vue</h2>
  </div>
  <script type="text/javascript">
    new Vue({
      el: '#app',
      data: {
        display: false,
        opacity: 1
      },
   	 // Vue完成模板的解析并把真实DOM元素放入页面后调此方法
      mounted(this) { 
        setInterval(() => {
          this.opacity -= 0.01
          // 注意 opacity 小于0的时候，处理...
          if(this.opacity <= 0 ) this.opacity = 1
        }, 16)
      },
    })
  </script>
```

2、生命周期钩子函数总结，她的一生是怎么样子的呐？？？？？？？？？？

![1_周期函数章结了](image/1_%E5%91%A8%E6%9C%9F%E5%87%BD%E6%95%B0%E7%AB%A0%E7%BB%93%E4%BA%86.png)

3、常用的生命周期钩子

- mounted: 发送ajax请求、启动定时器、绑定自定义事件、订阅消息等[初始化操作]。
- beforeDestroy; 清除定时器、解绑自定义事件、取消订阅消息等[收尾工作]。

4、关于销毁Vue实例I

- 销毁后借助Vue开发者工具看不到任何信息。
- 销毁后自定义事件会失效，但原生DOM事件依然有效。
- 一般不会在beforeDestroy操作数据，因为即便操作数据，也不会再触发更新流程了。

5、代码实现测试：

```html
 <div id="app">
   <h2>当前的n值是： {{n}}</h2>
   <button @click="add">点我n+1</button>
   <button @click="bye">点我销毁VM</button>
</div>
<script type="text/javascript">
    new Vue({
      el: '#app',
      // template:`<div></div>`,
      data: {
        n:1,
      },
      methods: {
        add() {
          this.n++
        },
        bye() {
          console.log('bye')
          // 别乱用bye bye。
          this.$destroy()
        },
        watch: {
          n(){
            console.log('n变了');
          }
        }
      },
      // 一、挂载流程
      // 1.无法访问data和methods方法
      beforeCreate() {
        console.log('beforeCreate')
        console.log(this)
        //   debugger
      },
      // 2。数据监测数据代理，可访问上面
      created() {
        console.log('created')
        console.log(this)
        //   debugger
      },
      // 3.呈现未经编译的DOM结构
      beforeMount() {
        console.log('beforeMount')
        console.log(this)
        //   debugger
      },
      // 4**. 经过Vue编译的DOM,初始化结束，
      // 一般做些网络请求，定时器自定义事件初始化等
      mounted() {
        console.log('mounted')
        console.log(this.$el instanceof HTMLElement)
        //   debugger
      },

      // 二、更新流程
      // 5. 数据是新的，页面是旧的，比如n=2
      beforeUpdate() {
        console.log('beforeUpdate')
        console.log(this.n)
      },
      // 6. 数据最新的，页面呈现的也是m=2
      updated() {
        console.log('updated')
        console.log(this.n)
      },

      // 三、销毁流程
      // 7**. data和method指令处于可用 (就别干别的事了)
      // 马上要执行销毁,用于关定时器，自定义事件收尾等等等
      beforeDestroy() {
        console.log('beforeDestroy')  
      },
      destroyed() {
        console.log('8，destroyed凉了',this,n)  
      },
      
    })
</script>
```

## 3. nextTick 好钩子

- 作用：在下一次 DOM 更新结束后执行其指定的回调 (this自动绑定当前实例) 
- 异步：当改变数据后，要基于更新后新DOM进行某些操作时，要在此钩子所指定的回调函数中执行。

## 4. Vue.set/$ 响应式

![2_响应式的原理](image/2_%E5%93%8D%E5%BA%94%E5%BC%8F%E7%9A%84%E5%8E%9F%E7%90%86.png)

Vue.set(target,key,val实现在new Vue实例后，再加数据也能响应式。

```html
 <div id="root">
        <h1>学生信息</h1>
        <button @click="student.age++">年龄+1岁</button> <br/>
        <button @click="addSex">添加性别属性，默认值：男</button> <br/>
        <button @click="student.sex = '未知' ">修改性别</button> <br/>
        <button @click="addFriend">在列表首位添加一个朋友</button> <br/>
        <button @click="updateFirstFriendName">
          修改第一个朋友的名字为：张三</button> <br/>
        <button @click="addHobby">添加一个爱好</button> <br/>
        <button @click="updateHobby">修改第一个爱好为：开车</button> <br/>
        <button @click="removeSmoke">过滤掉爱好中的抽烟</button> <br/>
        <h3>姓名：{{student.name}}</h3>
        <h3>年龄：{{student.age}}</h3>
        <h3 v-if="student.sex">性别：{{student.sex}}</h3>
        <h3>爱好：</h3>
        <ul>
            <li v-for="(h,index) in student.hobby" :key="index">
                {{h}}
            </li>
        </ul>
        <h3>朋友们：</h3>
        <ul>
            <li v-for="(f,index) in student.friends" :key="index">
                {{f.name}}--{{f.age}}
            </li>
        </ul>
    </div>
```

```js
  const vm = new Vue({
        el:'#root',
        data:{
            student:{
                name: 'tom',
                age: 22,
                hobby: ['抽烟','喝酒','烫头'],
                friends:[
                    {name:'jerry',age:35},
                    {name:'tony',age:36}
                ]
            }
        },
        methods: {
            addSex(){
                this.$set(this.student,'sex','男')
            },
            addFriend(){
                this.student.friends.unshift({name:'jack',age:66})
            },
            updateFirstFriendName(){
                this.student.friends[0].name = '张三'
            },
            addHobby(){
                this.student.hobby.push('学习')
            },
            updateHobby(){
                // this.student.hobby.splice(0,1,'开车')
                // Vue.set(this.student.hobby,0,'开车')
                this.$set(this.student.hobby,0,'开车')
            },
            removeSmoke(){
                this.student.hobby = this.student.hobby.filter((h)=>{
                    return h !== '抽烟'
               })
            }
        }
    })
```

## 5. Vue2 系数据代理

Vue中的数据代理：通过 vm 对象来代理 data 对象中属性的操作(读/写)

Vue中数据代理的优势：更加方便的操作 data 中的数据，基本原理：

- 通过Object.defineProperty()把data对象中所有属性添加到vm实例
- 为每一个添加到vm上的属性，都指定一个 getter/setter.

- 在 getter/setter内部去操作(读/写) data`中对应的属性。

- 回顾：Object. defineproperty 可以实现数据代理以及后续的数据劫持

- ```js
  // 需求: num 改变也要使对象的age属性值改变
      let number = 22
      let person = {
        name: 'xiaoli',
        sex: 'male',
        //  age: num, 这样num改变不会改变这里 
  }
  
      // 此时age属性不能被枚举，就是不能被遍历
      Object.defineProperty(person, 'age', { 
        // value: 22, 
        // 控制是否可枚举遍历
        // enumerable: true, 
        // 控制属性是否可被改
        // writable: true,
        // 属性是否可以被删除
        // configurable:true,
  
        //1. 当有人读取对象的age属性时，get函数就会被调用，且返回值就是age的值
        get() {
          console.log('每次访问会调用age');
          return number
        },
        //2.当有人修改对象的age属性时，set函数就会被调用， 且收到修改的具体值。。。
        set(value) {
          number = value
        }
      })
      console.log(person)
  ```

- 举例：通过一个对象代理对另一个对象中属性的操作(读/写)，如下方代理 obj2代理 obj1的x属性。

- ```js
    let obj1 = {x:100}
    let obj2 = {y:200}
    Object.defineProperty(obj2, 'x', { 
      get() {
        return obj1.x
      },
      set(value) {
        obj1.x = value
      }
    }...
  ```

## 6. Vue 监视数组方法

- 1、底层 Observer() 简单模拟监测对象的实现。

- ```js
  // 模拟个data数据对象，让它们监视吧
  let data = {
    name:'yulin',
    address:'秦皇岛',
  }
  
  // 创建一个监视的实例对象，监视变化
  const obs = new Observer(data)      
  
  // 准备一个vm实例对象，用于整合大家
  let vm = {}  
  vm._data = data = obs
  
  // 汇总对象中所有的属性形成一个数组
  function Observer(obj){
    const keys = Object.keys(obj)
    keys.forEach((k) => {
      Object.defineProperty(this, k, {
        get(){
          return obj[k]
        },
        set(val){
          console.log(`${k}被改了，我要去解析模板，
                              生成虚拟DOM.....我要开始忙了`)
          obj[k] = val
        }
      })
    })
  }   
  ```

**2、监视数据总结**

1. **Vue会监视 data 中所有层次的数据，那么如何监测对象中的数据？**

​             **通过 setter实现监视，且要在new Vue时就传入要监测的数据**

​                ***(1).对象中后追加的属性，Vue默认不做响应式处理***

​                ***(2).如需给后添加的属性做响应式，请使用如下API：***

​                        ***Vue.set(target，propertyName/index，value) 或*** 

​                        ***vm.$set(target，propertyName/index，value)***

2. **如何监测数组中的数据？**

​         **通过包裹数组更新元素的方法实现，本质就是做了两件事：**

​                  **(1).调用原生对应的方法对数组进行更新。**

​                  **(2). 重新解析模板，进而更新页面。**

**3. 监视数组方法总结**

 **在Vue修改数组中的某个元素一定要用如下方法，官方封装好的响应式**

​      **当数据发生变化时，会自动检测数据变化，视图会发生对应的更新**

- **push() 向数组末尾添加一个元素 +**
- **pop(): 删除数组中最后一个元素 -**
- **shift(): 删除数组中的第一个元素**  -
- **unshift(): 在数组最前面添加元素 +**
- **splice (start , del，item?)作用: 删除元素 / 插入元素 / 替换元素 增删改，就是切片有返回值**
  - **删除元素: 第二个参数传入你要删除几个元素(如果没有传,就删除后面所有的元素)**
  - **替换元素: 第二个参数, 表示要替换 (删)几个元素, 后面(加)是用于替换前面的元素**
  - **插入元素: 第二个参数，如果传入0，第三参数开始到 n个跟上要插入的元素既可以**

**注通过索引改数组不是响应式，没有监听。…………………………………………………………**

